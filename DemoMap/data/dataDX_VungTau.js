var data = [
 {
   "STT": 1,
   "Name": "Bệnh viện Tỉnh Bà Rịa - Vũng Tàu",
   "address": "Võ Văn Kiệt, Khu phố 2, Long Tâm, Phường Long Tâm, Thành phố Bà Rịa, Tỉnh Bà Rịa-Vũng Tàu, Việt Nam",
   "lat": 10.50942,
   "lon": 107.19700,
   "Rating": 3.98,
   "Number_of_beds": 600,
   "area": "Tỉnh"
 },
 {
   "STT": 2,
   "Name": "Bệnh viện Quân Dân Y",
   "address": "Lê Hữu Trác, Khu phố 4, Long Tâm, Phường Long Tâm, Thành phố Bà Rịa, Tỉnh Bà Rịa-Vũng Tàu, Việt Nam",
   "lat": 10.50810,
   "lon": 107.19418,
   "Rating": 3.98,
   "Number_of_beds": 600,
   "area": "Tỉnh"
 },
 {
   "STT": 3,
   "Name": "Bệnh viện Mắt Tỉnh Bà Rịa - Vũng Tàu",
   "address": "Phạm Ngọc Thạch, Phường Phước Hưng, Thành phố Bà Rịa, Tỉnh Bà Rịa-Vũng Tàu, Việt Nam",
   "lat": 10.50737,
   "lon": 107.17744,
   "Rating": 3.98,
   "Number_of_beds": 600,
   "area": "Tỉnh"
 }
];