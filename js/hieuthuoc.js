var data2 = [
  {
    "STT": 1,
    "Name": "\tQuầy thuốc Doanh Nghiệp Số 28 -Công Ty TNHH Thương Mại Thần Công Đại Việt\t",
    "address": "\tTổ 4 khu Bắc Sơn, phường Cẩm Sơn, thành phố Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.8437729,
    "Latitude": 106.2992912
  },
  {
    "STT": 2,
    "Name": "\tNhà thuốc Bệnh Viện Đa Khoa Cẩm Phả\t",
    "address": "\tBệnh viện đa khoa Cẩm Phả, đường Trần Phú, phường Cẩm Thành, thành phố Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0096076,
    "Latitude": 107.2768906
  },
  {
    "STT": 3,
    "Name": "\tQuầy thuốc Doanh Nghiệp Số 99 - Công Ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tThôn Đìa Mối, xã An Sinh, thị xã Đông Triều, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0958153,
    "Latitude": 106.6055534
  },
  {
    "STT": 4,
    "Name": "\tQuầy thuốc Doanh Nghiệp Số 20 - Công Ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tTổ 2 thôn Đồng Tâm, xã Lê Lợi, huyện Hoành Bồ, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0169087,
    "Latitude": 107.0421324
  },
  {
    "STT": 5,
    "Name": "\tNhà thuốc Hằng Ngọc\t",
    "address": "\tSố 172 Tuệ Tĩnh, phường Thanh Sơn, thành phố Uông Bí, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0399603,
    "Latitude": 106.7521665
  },
  {
    "STT": 6,
    "Name": "\tNhà thuốc Bệnh Viện Đa Khoa Tỉnh Quảng Ninh\t",
    "address": "\tBVĐK tỉnh ,  phố Tuệ Tĩnh, phường Bạch Đằng, thành phố Hạ Long, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9539294,
    "Latitude": 107.0884569
  },
  {
    "STT": 7,
    "Name": "\tQuầy thuốc Thúy Chinh 2 - Công Ty TNHH Dược Phẩm Hồng Dương\t",
    "address": "\tKi ốt số 2, chợ Cẩm Thạch, phường Cẩm Thạch, thành phố Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0101852,
    "Latitude": 107.2564208
  },
  {
    "STT": 8,
    "Name": "\tNhà thuốc Hương Sơn\t",
    "address": "\tSố nhà 54A phố 2, phường Mạo Khê, thị xã Đông Triều, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0598576,
    "Latitude": 106.5922003
  },
  {
    "STT": 9,
    "Name": "\tQuầy thuốc Doanh Nghiệp Hòa Liên - Công Ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tTổ 6 khu 10, thị trấn Trới, huyện Hoành Bồ, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0334558,
    "Latitude": 106.9887685
  },
  {
    "STT": 10,
    "Name": "\tQuầy thuốc Số 114 - Công Ty Cổ Phần Dược Vtyt Quảng Ninh\t",
    "address": "\tKm 16, chợ Trung Tâm, phố Hoàng Hoa Thám, thị trấn Quảng Hà, huyện Hải Hà, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.4498682,
    "Latitude": 107.7493787
  },
  {
    "STT": 11,
    "Name": "\tQuầy thuốc Doanh Nghiệp Kim Thùy - Công Ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tThôn 3, xã Hồng Thái Tây, thị xã Đông Triều, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0512964,
    "Latitude": 106.6717207
  },
  {
    "STT": 12,
    "Name": "\tQuầy thuốc Doanh Nghiệpanh Đào- Công Ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tThôn Thọ Tràng, xã Yên Thọ, thị xã Đông Triều, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.048418,
    "Latitude": 106.61684
  },
  {
    "STT": 13,
    "Name": "\tNhà thuốc Hồng Đức\t",
    "address": "\tSố 497 tổ 75 khu 7, phường Cửa Ông, thành phố Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.032741,
    "Latitude": 107.3598781
  },
  {
    "STT": 14,
    "Name": "\tNhà thuốc Tình Quân\t",
    "address": "\tSố nhà 396 tổ 1 khu 1, phường Hà Lầm, thành phố Hạ Long, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9533239,
    "Latitude": 107.0871119
  },
  {
    "STT": 15,
    "Name": "\tNhà thuốc Thái Hà\t",
    "address": "\tSố nhà 179 Nguyễn Văn Cừ, phường Hồng Hải, thành phố Hạ Long, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9563918,
    "Latitude": 107.0978244
  },
  {
    "STT": 16,
    "Name": "\tNhà thuốc Thanh Luyến\t",
    "address": "\tSố nhà 56 phố Vân Đồn khu 2, phường Trần Phú, thành phố Móng Cái, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.5301855,
    "Latitude": 107.9702169
  },
  {
    "STT": 17,
    "Name": "\tQuầy thuốc Dn Số 27 - Công Ty TNHHdp Hạ Long\t",
    "address": "\tTổ 1 khu 6 phường Mông Dương, thành phố Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.06284,
    "Latitude": 107.343399
  },
  {
    "STT": 18,
    "Name": "\tQuầy thuốc Dn Số 28 - Công Ty TNHHdp Hạ Long\t",
    "address": "\tThôn Yên Hòa, xã Yên Thọ, thị xã Đông Triều, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0567778,
    "Latitude": 106.6202887
  },
  {
    "STT": 19,
    "Name": "\tNhà thuốc Tâm Đức 9\t",
    "address": "\tTổ 43 khu 4, phường Cao Thắng, thành phố Hạ Long, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9664599,
    "Latitude": 107.096792
  },
  {
    "STT": 20,
    "Name": "\tNhà thuốc Hương Lý\t",
    "address": "\tSố 265, tổ 5 khu 3, phường Yết Kiêu, thành phố Hạ Long, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9600653,
    "Latitude": 107.0760905
  },
  {
    "STT": 21,
    "Name": "\tQuầy thuốc Dn Số 60 - Công Ty TNHHdp Bạch Đằng\t",
    "address": "\tKi ốt 159 Chợ Đầm Hà, thị trấn Đầm Hà, huyện Đầm Hà, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.3500114,
    "Latitude": 107.5992193
  },
  {
    "STT": 22,
    "Name": "\tQuầy thuốc Doanh Nghiệp Khánh Hòa - Công Ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tThôn Trại Thông, xã Bình Khê, thị xã Đông Triều, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.104795,
    "Latitude": 106.585353
  },
  {
    "STT": 23,
    "Name": "\tNhà thuốc Doanh Nghiệp Số 65 - Công Ty Cổ Phần Dược Vtyt Quảng Ninh\t",
    "address": "\tSố 06 đường Trần Hưng Đạo, phường Trần Hưng Đạo, thành phố Hạ Long, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9562358,
    "Latitude": 107.0851374
  },
  {
    "STT": 24,
    "Name": "\tQuầy thuốc Số 115 - Công Ty Cổ Phần Dược Vtyt Quảng Ninh\t",
    "address": "\tKi ốt chợ Bạch Đằng, phường Cẩm Thạch, thành phố Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0059919,
    "Latitude": 107.2479638
  },
  {
    "STT": 25,
    "Name": "\tNhà thuốc Minh Hải\t",
    "address": "\tTổ 2 khu 2, phường Mông Dương, thành phố Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.055278,
    "Latitude": 107.343056
  },
  {
    "STT": 26,
    "Name": "\tNhà thuốc An Phương\t",
    "address": "\tSố 63, tổ 86 khu 8, phường Cửa Ông, thành phố cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.032741,
    "Latitude": 107.3598781
  },
  {
    "STT": 27,
    "Name": "\tNhà thuốc Hạnh Loan\t",
    "address": "\tSố 556 Nguyễn Văn Cừ, phường Hồng Hà, thành phố Hạ Long, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9506193,
    "Latitude": 107.1325327
  },
  {
    "STT": 28,
    "Name": "\tNhà thuốc Thu Hằng\t",
    "address": "\tSố nhà 117 tổ 7, phường Cẩm Bình, thành phố Cẩm Phả, quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0002112,
    "Latitude": 107.2873282
  },
  {
    "STT": 29,
    "Name": "\tNhà thuốc Lâm Đoan\t",
    "address": "\tSố 365 đường Trần Phú, phường Cẩm Thành, thành phố Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0118793,
    "Latitude": 107.2790011
  },
  {
    "STT": 30,
    "Name": "\tNhà thuốc Hải Hòa\t",
    "address": "\tKhu 4, phường Hải Hòa, thành phố Móng Cái, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.532994,
    "Latitude": 107.9674392
  },
  {
    "STT": 31,
    "Name": "\tQuầy thuốc Doanh Nghiệp Mai Hiếu - Công Ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tSố 79 khu Vĩnh Thông, phường Mạo Khê, thị xã Đông Triều, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0598621,
    "Latitude": 106.5869951
  },
  {
    "STT": 32,
    "Name": "\tQuầy thuốc Doanh Nghiệp Cường Mai - Công Ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tThôn 8, xã Hải Đông, thành phố Móng Cái, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.5394395,
    "Latitude": 107.8835642
  },
  {
    "STT": 33,
    "Name": "\tQuầy thuốc Doanh Nghiệp Thanh Huyền - Công Ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tSố 190 Trần Bình Trọng, thị trấn Quảng Hà, huyện Hải Hà, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.4513441,
    "Latitude": 107.7561851
  },
  {
    "STT": 34,
    "Name": "\tQuầy thuốc Số 15 - Công Ty Cổ Phần Dược Vtyt Quảng Ninh\t",
    "address": "\tKm 7 khu 1 phường Hải Yên, thành phố Móng Cái, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.5239006,
    "Latitude": 107.9167514
  },
  {
    "STT": 35,
    "Name": "\tQuầy thuốc Doanh Nghiệp Bích Liên - Công Ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tKi ốt số 19 chợ Phương Đông, phường Phương Đông, thành phố Uông Bí, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.039657,
    "Latitude": 106.7248828
  },
  {
    "STT": 36,
    "Name": "\tNhà thuốc Số 2 - Công Ty Cổ Phần Thương Mại Và Dược Phẩm Nam Việt\t",
    "address": "\tTổ 13 khu 3, phường Hà Phong, thành phố Hạ Long, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.976522,
    "Latitude": 107.1543601
  },
  {
    "STT": 37,
    "Name": "\tNhà thuốc Minh Anh\t",
    "address": "\tSố 20, tổ 1 khu 7, phường Bãi Cháy, thành phố Hạ Long, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9643837,
    "Latitude": 107.0480378
  },
  {
    "STT": 38,
    "Name": "\tNhà thuốc Tâm Đức Iii\t",
    "address": "\tTổ 1 khu 5, phường Mông Dương, thành phố Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0822391,
    "Latitude": 107.3021069
  },
  {
    "STT": 39,
    "Name": "\tNhà thuốc Tâm An\t",
    "address": "\tSN 248 Đường Trần Phú, phường cao Xanh, Tphường Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9760021,
    "Latitude": 107.0854929
  },
  {
    "STT": 40,
    "Name": "\tNhà thuốc Thành Trung\t",
    "address": "\tSố 108 Tổ 20 khu 6, phường Quang Trung, Tphường Uông Bí, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.1057847,
    "Latitude": 106.8075136
  },
  {
    "STT": 41,
    "Name": "\tNhà thuốc Thuận Hưng - Công Ty Cổ Phần Dược Vtyt Quảng Ninh\t",
    "address": "\tSố 153 tổ 2 khu 4, phường Giếng Đáy, Tphường Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9815272,
    "Latitude": 107.0214647
  },
  {
    "STT": 42,
    "Name": "\tNhà thuốc Doanh Nghiệp Hồng Linh - Công Ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tSố 195, tổ 7 khu 3, phường Hồng Hà, Tphường Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9417785,
    "Latitude": 107.1277751
  },
  {
    "STT": 43,
    "Name": "\tNhà thuốc Viên Hồng\t",
    "address": "\tSố nhà 785 ,  Đường Trần Phú, phường Cẩm Thủy, Tphường cẩm Phả, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0098992,
    "Latitude": 107.2656074
  },
  {
    "STT": 44,
    "Name": "\tNhà thuốc Cường Long\t",
    "address": "\tSố 74 Kênh Liêm, phường Cao Thắng, Tphường Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9581057,
    "Latitude": 107.0921765
  },
  {
    "STT": 45,
    "Name": "\tQuầy thuốc Doanh Nghiệp Hoàng Hiếu - Công Ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tThôn Vĩnh Thái, xã Hồng Thái Đông, thị xã Đông Triều, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0518672,
    "Latitude": 106.683662
  },
  {
    "STT": 46,
    "Name": "\tQuầy thuốc Doanh Nghiệp Số 82 - Công Ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tXóm 1, thôn 1, xã Hiệp Hòa, thị xã Quảng Yên, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9387415,
    "Latitude": 106.796489
  },
  {
    "STT": 47,
    "Name": "\tQuầy thuốc Doanh Nghiệp Số 9- Công Ty TNHH Dược Phẩm Hồng Dương\t",
    "address": "\tTổ 2 khu 10, thị trấn Trới, huyện Hoành Bồ, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.02332,
    "Latitude": 106.988
  },
  {
    "STT": 48,
    "Name": "\tQuầy thuốc Doanh Nghiệp Hạnh Liên - Công Ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tSố 172, phường Vàng Danh, thành phố Uông Bí, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.1102653,
    "Latitude": 106.8060388
  },
  {
    "STT": 49,
    "Name": "\tQuầy thuốc Số 62 - Công Ty Cổ Phần Dược Vtyt Quảng Ninh\t",
    "address": "\tSố 62 Đường Triều Dương, phường Trần Phú, Tphường Móng Cái, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.5313545,
    "Latitude": 107.970876
  },
  {
    "STT": 50,
    "Name": "\tNhà thuốc Dn Hải Tiến- Công Ty TNHHdp Hạ Long\t",
    "address": "\tTổ 84 khu 8 phường Hà Khẩu, thành phố Hạ Long\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9819628,
    "Latitude": 106.9889904
  },
  {
    "STT": 51,
    "Name": "\tNhà thuốc Minh Anh 2\t",
    "address": "\tSố 173, tổ 1 khu 4, phường Giếng Đáy, Tphường Hạ Long, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9815272,
    "Latitude": 107.0214647
  },
  {
    "STT": 52,
    "Name": "\tNhà thuốc Doanh Nghiệp Số 10 Hùng Linh - Công Ty TNHH Dược Phẩm Bạch Đằng\t",
    "address": "\tSố 24, tổ 5 khu 2 Anh Đào, phường Bãi Cháy, Tphường Hạ Long, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9579074,
    "Latitude": 107.0226806
  },
  {
    "STT": 53,
    "Name": "\tQuầy thuốc Doanh Nghiệp Số 58 Dũng Thương - Công Ty TNHH Dược Phẩm Bạch Đằng\t",
    "address": "\tXã Quảng La, huyện Hoành Bồ, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0899707,
    "Latitude": 106.8849557
  },
  {
    "STT": 54,
    "Name": "\tQuầy thuốc Tâm Tâm - Công Ty TNHH Dược Phẩm Hải Bình\t",
    "address": "\tSN 519, tổ 2 khu 4, thị trấn Cái Rồng, huyện Vân Đồn, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.053875,
    "Latitude": 107.4351532
  },
  {
    "STT": 55,
    "Name": "\tCơ Sở thuốc Đông Y Gia Truyền Cụ Lang Lợi\t",
    "address": "\tSố nhà 528, tổ 9 khu 6, phường Giếng Đáy, Tphường Hạ Long, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9815272,
    "Latitude": 107.0214647
  },
  {
    "STT": 56,
    "Name": "\tCơ Sở Bán Lẻ thuốc Yhct\t",
    "address": "\tKi ốt số 21 + 22 chợ Trung Tâm Cẩm Phả ,  phường Cẩm Trung, Tphường Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0092042,
    "Latitude": 107.2730462
  },
  {
    "STT": 57,
    "Name": "\tNhà thuốc Minh Hà\t",
    "address": "\tSố 344 đường Minh Hà, tổ 3 khu 1, phường Hà Tu, thành phố Hạ Long, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9533239,
    "Latitude": 107.0871119
  },
  {
    "STT": 58,
    "Name": "\tNhà thuốc Đông Bắc - Công Ty TNHH Thương Mại Thần Công Đại Việt\t",
    "address": "\tSố 751 Nguyễn Văn Cừ, phường Hồng Hải, thành phố Hạ Long, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9463274,
    "Latitude": 107.1087437
  },
  {
    "STT": 59,
    "Name": "\tNhà thuốc Hùng Hằng - Công Ty TNHH Thương Mại Thần Công Đại Việt\t",
    "address": "\tTổ 5a Khu 4a, phường Cao Xanh, thành phố Hạ Long, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9722852,
    "Latitude": 107.0834734
  },
  {
    "STT": 60,
    "Name": "\tNhà thuốc Diệp Ngọc\t",
    "address": "\tTổ 30 khu 3, phường Cao Thắng, thành phố Hạ Long, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.96077,
    "Latitude": 107.0942
  },
  {
    "STT": 61,
    "Name": "\tNhà thuốc Trung Hiếu\t",
    "address": "\tSố 134 Nguyễn Văn Cừ, phường Hồng Hà, thành phố Hạ Long, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9413546,
    "Latitude": 107.1181626
  },
  {
    "STT": 62,
    "Name": "\tNhà thuốc Y Cao Hà Nội\t",
    "address": "\tSố 857 Đường Trần Phú, phường Cẩm Thạch, thành phố Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0101542,
    "Latitude": 107.2539561
  },
  {
    "STT": 63,
    "Name": "\tQuầy thuốc Doanh Nghiệp Số 158 - Công Ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tKi ốt 127 chợ Cái Rồng, thị trấn Cái Rồng, huyện Vân Đồn, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0681423,
    "Latitude": 107.4236013
  },
  {
    "STT": 64,
    "Name": "\tQuầy thuốc Doanh Nghiệp Linh Huệ - Công Ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tThôn Làng Đài, xã Đông Hải, huyện Tiên Yên, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.333565,
    "Latitude": 107.5201909
  },
  {
    "STT": 65,
    "Name": "\tNhà thuốc Minh Tâm\t",
    "address": "\tTổ 1 khu 1, phường Giếng Đáy, thành phố Hạ Long, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9815272,
    "Latitude": 107.0214647
  },
  {
    "STT": 66,
    "Name": "\tQuầy thuốc Doanh Nghiệp Hùng Yến - Công Ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tSố 30, Tổ 3 khu 10, thị trấn Trới, huyện Hoành Bồ, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9958136,
    "Latitude": 106.9705427
  },
  {
    "STT": 67,
    "Name": "\tQuầy thuốc Doanh Nghiệp Bích Liên 2 - Công Ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tSố 01 phố Đông Tiến, thị trấn Tiên Yên, huyện Tiên Yên, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.3337741,
    "Latitude": 107.4091219
  },
  {
    "STT": 68,
    "Name": "\tNhà thuốc Xuân Thủy\t",
    "address": "\tTổ 66 khu 8, phường Cao Thắng, thành phố Hạ Long, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.966594,
    "Latitude": 107.096763
  },
  {
    "STT": 69,
    "Name": "\tQuầy thuốc Số 26 - Công Ty Cp Dược Vtyt Quảng Ninh\t",
    "address": "\tSố 15 khu 5 tầng Phố Mới, phường Trần Hưng Đạo, Tphường  Hạ Long, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9562721,
    "Latitude": 107.0849499
  },
  {
    "STT": 70,
    "Name": "\tCơ Sở Bán Lẻ thuốc Y Học Cổ Truyền\t",
    "address": "\tSố nhà 48, tổ 7 khu 4A, phường Cẩm Trung, thành phố Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0020567,
    "Latitude": 107.269595
  },
  {
    "STT": 71,
    "Name": "\tQuầy thuốc Doanh Nghiệp Số 36 - Công Ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tKhu 1, phường Phong Hải, thị Xã Quảng Yên, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9372727,
    "Latitude": 106.8294239
  },
  {
    "STT": 72,
    "Name": "\tQuầy thuốc Doanh Nghiệp Vũ Loan - Công Ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tSố 4 ,  phố Chu Văn An, thị trấn Đầm hà, huyện Đầm hà, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.3504791,
    "Latitude": 107.6019029
  },
  {
    "STT": 73,
    "Name": "\tNhà thuốc Trinh Hương\t",
    "address": "\tSố nhà 17, đườngTrần Phú, phường Cẩm Tây, Tphường Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0129841,
    "Latitude": 107.2824314
  },
  {
    "STT": 74,
    "Name": "\tQuầy thuốc Dn Tùng Dung - Công Ty TNHHdp Hạ Long\t",
    "address": "\tThôn 10A, xã Hải Xuân, Tphường Móng Cái, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.4982894,
    "Latitude": 107.9828138
  },
  {
    "STT": 75,
    "Name": "\tQuầy thuốc Bệnh Viện Vân Đồn- Công Ty TNHH Dược Phẩm Hồng Dương\t",
    "address": "\tBVĐK Vân Đồn,  Thôn 12, xã Hạ Long, huyện Vân Đồn, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.8758105,
    "Latitude": 107.4944308
  },
  {
    "STT": 76,
    "Name": "\tQuầy thuốc Ngọc Bích - Công Ty TNHH Dược Phẩm Hồng Dương\t",
    "address": "\txã Liên Vị, thị xã Quảng Yên, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.8625707,
    "Latitude": 106.8001396
  },
  {
    "STT": 77,
    "Name": "\tQuầy thuốc Hoa Phú - Công Ty TNHH Dược Phẩm Hồng Dương\t",
    "address": "\tThôn Lục Nà, xã Lục Hồn, huyện Bình Liêu, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.5523751,
    "Latitude": 107.4270206
  },
  {
    "STT": 78,
    "Name": "\tQuầy thuốc Hùng Thảo - Công Ty TNHH Dược Phẩm Hồng Dương\t",
    "address": "\tSố 450, tổ 2 khu 7, thị trấn Trới, huyện Hoành Bồ, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0256819,
    "Latitude": 106.9983133
  },
  {
    "STT": 79,
    "Name": "\tQuầy thuốc Doanh Nghiệp Bích Thủy - Công Ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tSố 120, tổ 22 khu 7, phường Vàng Danh, thành phố Uông Bí, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.1102653,
    "Latitude": 106.8060388
  },
  {
    "STT": 80,
    "Name": "\tNhà thuốc Yến Thanh\t",
    "address": "\tSố nhà 31, Tổ 2 Khu phố Hòa Bình, phường Cẩm Tây, thành phố Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.015282,
    "Latitude": 107.2851737
  },
  {
    "STT": 81,
    "Name": "\tQuầy thuốc Tâm Tâm - Công Ty TNHH Thương Mại Thần Công Đại Việt\t",
    "address": "\tSố nhà 519, tổ 2 khu 4, thị trấn Cái Rồng, huyện Vân Đồn, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.053875,
    "Latitude": 107.4351532
  },
  {
    "STT": 82,
    "Name": "\tQuầy thuốc Doanh Nghiệp Công Dũng - Công Ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tThôn Cẩm Thành, xã Cẩm La, thị xã Quảng Yên, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9048714,
    "Latitude": 106.8104256
  },
  {
    "STT": 83,
    "Name": "\tQuầy thuốc Doanh Nghiệp Ninh Giang - Công Ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tKi ốt Chợ km 9 Quang Hanh, Tphường Cẩm Phả, Quảnh Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0082107,
    "Latitude": 107.2271147
  },
  {
    "STT": 84,
    "Name": "\tQuầy thuốc Doanh Nghiệp Chiến Nhung - Công Ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tKi ốt Chợ km 9 Quang Hanh, Tphường Cẩm Phả, Quảnh Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0082107,
    "Latitude": 107.2271147
  },
  {
    "STT": 85,
    "Name": "\tQuầy thuốc Doanh Nghiệp Minh Tiến - Công Ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tThôn Tràng Bảng, xã Tràng An, thị xã Đông Triều, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0968826,
    "Latitude": 106.572189
  },
  {
    "STT": 86,
    "Name": "\tQuầy thuốc Dn Số 62 - Công Ty TNHHdp Bạch Đằng\t",
    "address": "\tThôn Tân Yên, xã Hồng Thái Đông, thị xã Đông Triều, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0591112,
    "Latitude": 106.6918802
  },
  {
    "STT": 87,
    "Name": "\tNhà thuốc Ngọc Hà\t",
    "address": "\tA7 nhà A, chung cư cột 8, phường Hồng Hà, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.937282,
    "Latitude": 107.1256712
  },
  {
    "STT": 88,
    "Name": "\tNhà thuốc Hoa Lệ\t",
    "address": "\tSố nhà 57, tổ 2 khu 4, phường Hà Lầm, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9579074,
    "Latitude": 107.0226806
  },
  {
    "STT": 89,
    "Name": "\tNhà thuốc Thu Huyền\t",
    "address": "\tSố 183 đường Hạ Long, tổ 1 khu 4, phường Giếng Đáy, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.1534257,
    "Latitude": 106.1509887
  },
  {
    "STT": 90,
    "Name": "\tNhà thuốc Số 9\t",
    "address": "\tSố nhà 87 Kênh Liêm, phường Bạch Đằng, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9582493,
    "Latitude": 107.0917291
  },
  {
    "STT": 91,
    "Name": "\tQuầy thuốc Doanh Nghiệp Thuận Tâm - Công Ty Cổ Phần Thương Mại Và Dược Phẩm Nam Việt\t",
    "address": "\tTổ 6 khu Công Nông, thị trấn Mạo Khê, huyện Đông Triều, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0675098,
    "Latitude": 106.5996595
  },
  {
    "STT": 92,
    "Name": "\tQuầy thuốc Số 6 - Công Ty TNHH Dược Phẩm Thương Mại Thái Ngọc\t",
    "address": "\tKi ốt chợ Hoành Bồ khu 10, thị trấn Trới, huyện Hoành Bồ, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0235233,
    "Latitude": 106.9880336
  },
  {
    "STT": 93,
    "Name": "\tQuầy thuốc Số 121 - Công Ty Cổ Phần Dược Vtyt Quảng Ninh\t",
    "address": "\tKhu 5, phường Hải Hòa, thành phố Móng Cái, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.517982,
    "Latitude": 108.0218071
  },
  {
    "STT": 94,
    "Name": "\tQuầy thuốc Số 52 - Công Ty Cổ Phần Dược Vtyt Quảng Ninh\t",
    "address": "\tSố nhà 168, đường Trần Phú, thành phố Cẩm Phả, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.012772,
    "Latitude": 107.283515
  },
  {
    "STT": 95,
    "Name": "\tQuầy thuốc Số 09 - Công Ty Cổ Phần Dược Vtyt Quảng Ninh\t",
    "address": "\tSố 113, tổ 1 khu 1, phường Hà Trung, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9533239,
    "Latitude": 107.0871119
  },
  {
    "STT": 96,
    "Name": "\tQuầy thuốc Số 33 - Công Ty Cổ Phần Dược Vtyt Quảng Ninh\t",
    "address": "\tKi ốt số 122 chợ Hồng Hà, phường Hồng Hà, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9479461,
    "Latitude": 107.1295067
  },
  {
    "STT": 97,
    "Name": "\tCơ Sở Kinh Doanh thuốc Đông Y Thanh Thảo\t",
    "address": "\tKi ốt số 224 chợ Trung Tâm, thị trấn  Mạo Khê, Huyện Đông Triều, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0675098,
    "Latitude": 106.5996595
  },
  {
    "STT": 98,
    "Name": "\tCơ Sở Bán Lẻ thuốc Đông Y\t",
    "address": "\tTổ 60 khu 7, phường Cao Thắng, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.96472,
    "Latitude": 107.0978
  },
  {
    "STT": 99,
    "Name": "\tQuầy thuốc Hà Anh - Công Ty TNHH Dược Phẩm Hồng Dương\t",
    "address": "\tChợ Mới Kim Sen, xã Kim Sơn, huyện Đông Triều, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0719973,
    "Latitude": 106.5686164
  },
  {
    "STT": 100,
    "Name": "\tQuầy thuốc Sơn Phú - Công Ty TNHH Dược Phẩm Hồng Dương\t",
    "address": "\tThôn Nhuệ Hổ, xã Kim Sơn, huyện Đông Triều, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.070062,
    "Latitude": 106.5553799
  },
  {
    "STT": 101,
    "Name": "\tQuầy thuốc Doanh Nghiệp Hoàng Trang - Công Ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tThôn 1, xã Quảng Phong, huyện Hải Hà, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.4290946,
    "Latitude": 107.7155099
  },
  {
    "STT": 102,
    "Name": "\tNhà thuốc Tuấn Hòa\t",
    "address": "\tSố 192 đường Quang Trung, phường Quang Trung, thành phố Uông Bí, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0340907,
    "Latitude": 106.7756767
  },
  {
    "STT": 103,
    "Name": "\tQuầy thuốc Số 7 Nguyễn Toàn - Công Ty TNHH Dược Phẩm Tm Thái Ngọc\t",
    "address": "\tSố 98 Vĩnh Thông, thị trấn Mạo Khê, huyện Đông Triều, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.056883,
    "Latitude": 106.598092
  },
  {
    "STT": 104,
    "Name": "\tQuầy thuốc Doanh Nghiệp Vũ Hà- Công Ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tTổ 1 khu Vĩnh Tuy 1, thị trấn Mạo Khê, huyện Đông Triều, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0675098,
    "Latitude": 106.5996595
  },
  {
    "STT": 105,
    "Name": "\tQuầy thuốc Doanh Nghiệp Tâm Ba - Công Ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tSN 48 khu phố 2, thị trấn Mạo Khê, huyện Đông Triều, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0584203,
    "Latitude": 106.5985501
  },
  {
    "STT": 106,
    "Name": "\tDsthuyện  Nguyễn Thị Quyết\t",
    "address": "\tThôn Cẩm Thành, xã Cẩm La, thị xã Quảng Yên, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9048714,
    "Latitude": 106.8104256
  },
  {
    "STT": 107,
    "Name": "\tNhà thuốc Mạnh Thanh - Công Ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tKhu Đường Ngang, phường Minh Thành, thị xã Quảng Yên, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.00493,
    "Latitude": 106.8552697
  },
  {
    "STT": 108,
    "Name": "\tQuầy thuốc Doanh Nghiệp Số 7 - Công Ty Cổ Phần Thương Mại Và Dược Phẩm Nam Việt\t",
    "address": "\tTổ 6 khu Vĩnh Phú, phường Mạo Khê, thị xã Đông Triều, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0584203,
    "Latitude": 106.5985501
  },
  {
    "STT": 109,
    "Name": "\tQuầy thuốc Doanh Nghiệp Số 32 - Công Ty TNHH Dược Phẩm Bạch Đằng\t",
    "address": "\tSố 8, tổ 2 khu 10, thị trấn Trới, huyện Hoành Bồ, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0256819,
    "Latitude": 106.9983133
  },
  {
    "STT": 110,
    "Name": "\tNhà thuốc Linh Chi\t",
    "address": "\tTổ 5 khu 5B, phường Bãi Cháy, thành phố Hạ Long, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9606657,
    "Latitude": 107.0613154
  },
  {
    "STT": 111,
    "Name": "\tQuầy thuốc Doanh Nghiệp Số 10 - Công Ty Cổ Phần Thương Mại Và Dược Phẩm Nam Việt\t",
    "address": "\tThôn 4, xã Hoàng Tân, thị xã Quảng Yên, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9135556,
    "Latitude": 106.9240554
  },
  {
    "STT": 112,
    "Name": "\tQuầy thuốc Doanh Nghiệp Thanh Thùy - Công Ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tTổ 3 khu Vĩnh Hồng, thị trấn Mạo Khê, huyện Đông Triều, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0675098,
    "Latitude": 106.5996595
  },
  {
    "STT": 113,
    "Name": "\tQuầy thuốc Doanh Nghiệp Số 140 - Công Ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tThôn 6, xã Đức Chính, huyện Đông Triều, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0890514,
    "Latitude": 106.5192566
  },
  {
    "STT": 114,
    "Name": "\tQuầy thuốc Số 136 - Công Ty Cp Dược Vtyt Quảng Ninh\t",
    "address": "\tSố 140, tổ 8 khu1, phường Hà Tu, Tphường  Hạ Long, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9546746,
    "Latitude": 107.1425442
  },
  {
    "STT": 115,
    "Name": "\tNhà thuốc Kết Nam\t",
    "address": "\tSố nhà 859, tổ 6 khu 10, phường  Hồng Hải,Tphường Hạ Long, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9499748,
    "Latitude": 107.1041465
  },
  {
    "STT": 116,
    "Name": "\tNhà thuốc Quang Minh\t",
    "address": "\tSN 328, tổ 3 khu2, phường Yết Kiêu, Tphường Hạ Long, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9600653,
    "Latitude": 107.0760905
  },
  {
    "STT": 117,
    "Name": "\tQuầy thuốc Doanh Nghiệp Số 26 - Công Ty TNHH Dược Phẩm Hồng Dương\t",
    "address": "\tTổ 11 khu 4, phường  Hùng Thắng, Tphường  Hạ Long, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9576774,
    "Latitude": 107.0007987
  },
  {
    "STT": 118,
    "Name": "\tQuầy thuốc Minh Hằng - Công Ty TNHH Dược Phẩm Hồng Dương\t",
    "address": "\tSN 19 tổ 1, khu Vĩnh Lập,thị trấn  Mạo Khê, huyện Đông Triều, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0598621,
    "Latitude": 106.5869951
  },
  {
    "STT": 119,
    "Name": "\tQuầy thuốc Ngọc Liên - Công Ty TNHH Dược Phẩm Hồng Dương\t",
    "address": "\tSố 82, Hoàng Hoa Thám, thị trấn Mạo Khê, huyện  Đông Triều, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0569546,
    "Latitude": 106.6040799
  },
  {
    "STT": 120,
    "Name": "\tQuầy thuốc Sạc Chi - Công Ty TNHH Dược Phẩm Hồng Dương\t",
    "address": "\tTổ 1 khu Vĩnh Quang, thị trấn Mạo Khê, huyện  Đông Triều, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0584203,
    "Latitude": 106.5985501
  },
  {
    "STT": 121,
    "Name": "\tQuầy thuốc Số 29 - Công Ty TNHH Dược Phẩm Hồng Dương\t",
    "address": "\tThôn Đông Ngũ, xã Đông Ngũ, huyện Tiên Yên, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.337168,
    "Latitude": 107.4618143
  },
  {
    "STT": 122,
    "Name": "\tQuầy thuốc Thành Thủy - Công Ty TNHH Dược Phẩm Hồng Dương\t",
    "address": "\tThôn 5, xã Quảng Long, huyện Hải Hà, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.4612614,
    "Latitude": 107.6951905
  },
  {
    "STT": 123,
    "Name": "\tQuầy thuốc Thảo Hiền - Công Ty TNHH Dược Phẩm Hồng Dương\t",
    "address": "\tKi ốt chợ Trới, thị trấn Trới, huyện Hoành Bồ, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0281241,
    "Latitude": 106.9889904
  },
  {
    "STT": 124,
    "Name": "\tQuầy thuốc Thành Hương - Công Ty TNHH Dược Phẩm Hồng Dương\t",
    "address": "\tThôn 1, xóm 1, xã Hiệp Hòa, thị xã Quảng Yên, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9387415,
    "Latitude": 106.796489
  },
  {
    "STT": 125,
    "Name": "\tCông Ty Trách Nhiệm Hữu Hạn Dược Phúc Việt\t",
    "address": "\tSố 216 đường Bãi Muối, phường Cao Thắng, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9687487,
    "Latitude": 107.0970687
  },
  {
    "STT": 126,
    "Name": "\tQuầy thuốc Doanh Nghiệp Phương Thùy- Công Ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tSố 103 khu 2, phường Đông Triều, thị xã Đông Triều, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0836091,
    "Latitude": 106.514091
  },
  {
    "STT": 127,
    "Name": "\tQuầy thuốc Dn Số 31 - Công Ty TNHHdp Bạch Đằng\t",
    "address": "\tKm 11, Phường Minh Thành, thị xã Quảng Yên, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.00493,
    "Latitude": 106.8552697
  },
  {
    "STT": 128,
    "Name": "\tNhà thuốc Số 6 - Công Ty Cổ Phần Dược Vtyt Quảng Ninh\t",
    "address": "\tKi ốt chợ Trung Tâm Cẩm Phả, phường Cẩm Trung, thành phố Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0079046,
    "Latitude": 107.2705186
  },
  {
    "STT": 129,
    "Name": "\tCông Ty TNHH Thương Mại Thàn Công Đại Việt\t",
    "address": "\tSố 58 tổ 3 khu 6, phường Hồng Hải, thành phố Hạ Long, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9590927,
    "Latitude": 107.0728813
  },
  {
    "STT": 130,
    "Name": "\tNhà thuốc An An - Công Ty Cổ Phần Dược Vtyt Quảng Ninh\t",
    "address": "\tTổ 66 khu 8, đường Bãi Muối, phường Cao Thắng, thành phố Hạ Long, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9687487,
    "Latitude": 107.0970687
  },
  {
    "STT": 131,
    "Name": "\tNhà thuốc Thúy Hằng 2\t",
    "address": "\tSN 02 tổ 1 Minh Khai, phường Đại Yên, Tphường Hạ Long, Quảng Ninhuyện \t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9649885,
    "Latitude": 106.9432389
  },
  {
    "STT": 132,
    "Name": "\tNhà thuốc Doanh Nghiệp Số 12 Minh Đức - Công Ty TNHH Dược Phẩm Bạch Đằng\t",
    "address": "\tSố 560, tổ 57 khu 6, phường Cao Thắng, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9590927,
    "Latitude": 107.0728813
  },
  {
    "STT": 133,
    "Name": "\tNhà thuốc Ba Chẽ\t",
    "address": "\tKhu 2, thị trấn Ba Chẽ, huyện Ba Chẽ, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.2718654,
    "Latitude": 107.2836989
  },
  {
    "STT": 134,
    "Name": "\tQuầy thuốc Doanh Nghiệp Phượng Hồng - Công Ty Cổ Phần Thương Mại Và Dược Phẩm Nam Việt\t",
    "address": "\tThôn 5, xã Sông Khoai, thị xã Quảng Yên, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9768581,
    "Latitude": 106.8119382
  },
  {
    "STT": 135,
    "Name": "\tQuầy thuốc Số 32 - Công Ty Cp Dược Vtyt Quảng Ninh\t",
    "address": "\tTổ 7 khu 2, phường Giếng Đáy, Tphường  Hạ Long, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9815961,
    "Latitude": 107.0210624
  },
  {
    "STT": 136,
    "Name": "\tNhà thuốc 262 - Công Ty Cổ Phần Dược Vtyt Quảng Ninh\t",
    "address": "\tSố 262 ,  Đường Cao Xanh, phường Cao Xanh, Tphường Hạ Long, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9744635,
    "Latitude": 107.081176
  },
  {
    "STT": 137,
    "Name": "\tQuầy thuốc Số 03 - Công Ty Cp Dược Vtyt Quảng Ninh\t",
    "address": "\tBệnh viện đa khoa khu vực Móng Cái, TP Móng Cái, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.5249181,
    "Latitude": 107.9610462
  },
  {
    "STT": 138,
    "Name": "\tQuầy thuốc Quang Minh - Công Ty TNHH Dược Phẩm Hồng Dương\t",
    "address": "\tSố 88 Chợ Cột, thị trấn Đông Triều, huyện Đông Triều, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0836378,
    "Latitude": 106.5161416
  },
  {
    "STT": 139,
    "Name": "\tQuầy thuốc Doanh Nghiệp Phương Thảo - Công Ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tThôn Gia Mô, xã Kim Sơn, huyện Đông Triều, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0707941,
    "Latitude": 106.5642407
  },
  {
    "STT": 140,
    "Name": "\tQuầy thuốc Doanh Nghiệp Phương Thảo - Công Ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tThôn Gia Mô, xã Kim Sơn, huyện Đông Triều, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0707941,
    "Latitude": 106.5642407
  },
  {
    "STT": 141,
    "Name": "\tQuầy thuốc Đức Chung - Công Ty TNHH Dược Phẩm Hồng Dương\t",
    "address": "\tSN 665,  Đường Nguyễn Đức Cảnh, phường Quang Hanh, Tphường  Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0072997,
    "Latitude": 107.2255782
  },
  {
    "STT": 142,
    "Name": "\tNhà thuốc Bảo Chinh\t",
    "address": "\tSố 103 B,  Hùng Vương, phường Ka Long, thành phố Móng Cái, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.5271014,
    "Latitude": 107.9711471
  },
  {
    "STT": 143,
    "Name": "\tNhà thuốc Thanh Chương\t",
    "address": "\tSố nhà 22, phố Trần Bình Trọng, thị trấn Quảng Hà , huyện Hải Hà, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.452286,
    "Latitude": 107.7521534
  },
  {
    "STT": 144,
    "Name": "\tQuầy thuốc Số 22 - Công Ty Cổ Phần Dược Vật Tư Y Tế Quảng Ninh\t",
    "address": "\tSố 330,  Đường Hà Lầm, phường Hà Trung, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9585894,
    "Latitude": 107.1223572
  },
  {
    "STT": 145,
    "Name": "\tQuầy thuốc Số 12 - Công Ty Cổ Phần Dược Vtyt Quảng Ninh\t",
    "address": "\tSố 9, đường Trần Phú, phường Trần Phú, thành phố Móng Cái, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.5353709,
    "Latitude": 107.9724429
  },
  {
    "STT": 146,
    "Name": "\tQuầy thuốc Số 18 - Công Ty Cổ Phần Dược Vtyt Quảng Ninh\t",
    "address": "\tSố 108B, đường Hùng Vương, thành phố Móng Cái, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.5278509,
    "Latitude": 107.9703632
  },
  {
    "STT": 147,
    "Name": "\tQuầy thuốc Số 44 - Công Ty Cổ Phần Dược Vtyt Quảng Ninh\t",
    "address": "\tKi ốt số 15, chợ Suối Khoáng, phường Quang Hanh, thành phố Cẩm Phả, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9846214,
    "Latitude": 107.1953201
  },
  {
    "STT": 148,
    "Name": "\tQuầy thuốc Số 55 - Công Ty Cổ Phần Dược Vtyt Quảng Ninh\t",
    "address": "\tSố nhà 86, tổ 18 A khu 6, phường Quang Trung, thành phố Uông Bí, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.1057847,
    "Latitude": 106.8075136
  },
  {
    "STT": 149,
    "Name": "\tQuầy thuốc Số 27 - Công Ty Cổ Phần Dược Vtyt Quảng Ninh\t",
    "address": "\tSố 53 phố Anh Đào, phường Bãi Cháy, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9596227,
    "Latitude": 107.050913
  },
  {
    "STT": 150,
    "Name": "\tNhà thuốc Dn Dương Dịu - Công Ty Cptm Và Dp Nam Việt\t",
    "address": "\tSố nhà 904 tổ 109 khu 10B, phường Cửa Ông, Tphường Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.032741,
    "Latitude": 107.3598781
  },
  {
    "STT": 151,
    "Name": "\tQuầy thuốc Số 82 - Công Ty Cổ Phần Dược Vtyt Quảng Ninh\t",
    "address": "\tKi ốt số 12 chợ Cẩm Đông, phường cẩm Đông, thành phố Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0081551,
    "Latitude": 107.2919065
  },
  {
    "STT": 152,
    "Name": "\tQuầy thuốc Số 85 - Công Ty Cổ Phần Dược Vtyt Quảng Ninh\t",
    "address": "\tSố 369 Đường Trần Phú, phường Cẩm Thành, thành phố Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0118793,
    "Latitude": 107.2790011
  },
  {
    "STT": 153,
    "Name": "\tQuầy thuốc Số 80 - Công Ty Cổ Phần Dược Vtyt Quảng Ninh\t",
    "address": "\tSố 383 tổ 10G, phường Cẩm Thịnh, thành phố Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9914726,
    "Latitude": 107.3434916
  },
  {
    "STT": 154,
    "Name": "\tNhà thuốc Số 4 - Công Ty TNHH Dược Phẩm Bạch Đằng\t",
    "address": "\tSố 264 Nguyễn Văn Cừ, phường Hồng Hải, thành phố Hạ Long, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9411638,
    "Latitude": 107.1214625
  },
  {
    "STT": 155,
    "Name": "\tNhà thuốc Phạm Thị Hồng Oanh\t",
    "address": "\tSố nhà 105, đường Cao Thắng, phường Cao Thắng, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9628713,
    "Latitude": 107.0962345
  },
  {
    "STT": 156,
    "Name": "\tQuầy thuốc Số 48 - Công Ty Cổ Phần Dược Vtyt Quảng Ninh\t",
    "address": "\tChợ Quang Trung, thành phố Uông Bí, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0347844,
    "Latitude": 106.769557
  },
  {
    "STT": 157,
    "Name": "\tNhà thuốc Tiên Yên\t",
    "address": "\tSố nhà 228 ,  Phố Thống Nhất, Thị trấn Tiên Yên, huyện Tiên yên, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.3289859,
    "Latitude": 107.3966896
  },
  {
    "STT": 158,
    "Name": "\tQuầy thuốc Doanh Nghiệp Thanh Hà - Công Ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tSố 131 đường Hoàng Hoa Thám, thị trấn Mạo Khê, huyện Đông Triều, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0584203,
    "Latitude": 106.5985501
  },
  {
    "STT": 159,
    "Name": "\tQuầy thuốc Số 42 - Công Ty Cổ Phần Dược Vật Tư Y Tế Quảng Ninh\t",
    "address": "\tSố 26, tổ 4 khu 9, phường Bãi Cháy, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9643837,
    "Latitude": 107.0480378
  },
  {
    "STT": 160,
    "Name": "\tQuầy thuốc Số 30 - Công Ty Cổ Phần Dược Vật Tư Y Tế Quảng Ninh\t",
    "address": "\tSố 437, đường Hạ Long, phường Bãi Cháy, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9547931,
    "Latitude": 106.9712112
  },
  {
    "STT": 161,
    "Name": "\tQuầy thuốc Số 58 - Công Ty Cổ Phần Dược Vật Tư Y Tế Quảng Ninh\t",
    "address": "\tSố 1044 ,  đường Trần Phú, phường Cẩm Thạch, thành phố Cẩm Phả, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0101542,
    "Latitude": 107.2539561
  },
  {
    "STT": 162,
    "Name": "\tQuầy thuốc Số 41 - Công Ty Cổ Phần Dược Vật Tư Y Tế Quảng Ninh\t",
    "address": "\tSố 194 đường Trần Phú, phường Cẩm Thành, thành phố Cẩm Phả, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0118793,
    "Latitude": 107.2790011
  },
  {
    "STT": 163,
    "Name": "\tNhà thuốc Thảo Linh - Công Ty Cổ Phần Dược Vật Tư Y Tế Quảng Ninh\t",
    "address": "\tTổ 1 khu 4, phường Hồng Hà, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9390906,
    "Latitude": 107.125438
  },
  {
    "STT": 164,
    "Name": "\tQuầy thuốc Số 10 - Công Ty Cổ Phần Dược Vật Tư Y Tế Quảng Ninh\t",
    "address": "\tKhu 8, phường Hải Hòa, thành phố Móng Cái, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.5352465,
    "Latitude": 108.0124469
  },
  {
    "STT": 165,
    "Name": "\tCơ Sở Kinh Doanh thuốc Yhct Kim Ngân - Cơ Sở 2\t",
    "address": "\tKi ốt số 8, chợ trung Tâm Mạo Khê, huyện Đông Triều, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0675098,
    "Latitude": 106.5996595
  },
  {
    "STT": 166,
    "Name": "\tNhà thuốc Hưng Thịnh\t",
    "address": "\tSố 58, tổ 2B khu 1, phường Hồng Hà, Tphường Hạ Long, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9533239,
    "Latitude": 107.0871119
  },
  {
    "STT": 167,
    "Name": "\tQuầy thuốc Doanh Nghiệp Hạnh Tùng - Công Ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tTrung tâm thương mại khu đô thị mới, xã Kim Sơn, huyện Đông Triều, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.066915,
    "Latitude": 106.5694561
  },
  {
    "STT": 168,
    "Name": "\tQuầy thuốc Doanh Nghiệp Kiên Tuyền - Công Ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tTổ 5 Hoàng Hoa Thám, thị trấn Mạo Khê, huyện Đông Triều, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0584203,
    "Latitude": 106.5985501
  },
  {
    "STT": 169,
    "Name": "\tQuầy thuốc Doanh Nghiệp Mai Hoa - Công Ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tThôn 4, xã Đức Chính, huyện Đông Triều, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0898979,
    "Latitude": 106.5201084
  },
  {
    "STT": 170,
    "Name": "\tQuầy thuốc Doanh Nghiệp Bảo Châu - Công Ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tThôn Khê Hạ, xã Việt Dân, huyện Đông Triều, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0958153,
    "Latitude": 106.6055534
  },
  {
    "STT": 171,
    "Name": "\tNhà thuốc Huy Hiếu\t",
    "address": "\tSố 36 phố Hải Phong, phường Hồng Hải, Tphường Hạ Long, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9417784,
    "Latitude": 107.1096306
  },
  {
    "STT": 172,
    "Name": "\tQuầy thuốc Doanh Nghiệp Số 9 - Công Ty TNHH Dược Phẩm Bạch Đằng\t",
    "address": "\tChợ cái rồng, khu 8, thị trấn Cái Rồng, huyện Vân Đồn, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.065121,
    "Latitude": 107.424283
  },
  {
    "STT": 173,
    "Name": "\tQuầy thuốc Dn Số 48 - Công Ty TNHHdp Bạch Đằng\t",
    "address": "\tChợ Km 11, Phường Minh Thành, thị xã Quảng Yên, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.00493,
    "Latitude": 106.8552697
  },
  {
    "STT": 174,
    "Name": "\tNhà thuốc Hưng Thịnh\t",
    "address": "\tSố 74, tổ 2 khu 1, phường Hồng Hà, thành phố Hạ Long, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9533239,
    "Latitude": 107.0871119
  },
  {
    "STT": 175,
    "Name": "\tNhà thuốc Hồng Thân\t",
    "address": "\tKi ốt số 3 chợ Hà Lầm, phường Hà Lầm, Tphường Hạ Long, QuảngNinh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9666167,
    "Latitude": 107.108266
  },
  {
    "STT": 176,
    "Name": "\tNhà thuốc Số 16\t",
    "address": "\tSố nhà 16, tổ 3 khu 1, phường Cao Xanh, Tphường Hạ Long, Q. Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9533239,
    "Latitude": 107.0871119
  },
  {
    "STT": 177,
    "Name": "\tCông Ty TNHH Dược Phẩm Hồng Dương\t",
    "address": "\tSố 1, tổ 1 khu 1, phường Trần Hưng Đạo, Tphường Hạ Long, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9533239,
    "Latitude": 107.0871119
  },
  {
    "STT": 178,
    "Name": "\tNhà thuốc Mai Huyền - Công Ty Cổ Phần Dược Vtyt Quảng Ninh\t",
    "address": "\tSố 130 Đường Cao Xanh, phường Cao Xanh, Tphường Hạ Long, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9744635,
    "Latitude": 107.081176
  },
  {
    "STT": 179,
    "Name": "\tQuầy thuốc Số 52- Công Ty Cổ Phần Dược Vtyt Quảng Ninh\t",
    "address": "\tSố 153, tổ 1 khu Nam Trung, phường Nam Khê, Tphường Hạ Long, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0166472,
    "Latitude": 106.8231871
  },
  {
    "STT": 180,
    "Name": "\tQuầy thuốc Số 31- Công Ty Cổ Phần Dược Vtyt Quảng Ninh\t",
    "address": "\tKi ốt số 6, chợ Trung Tâm, Tphường  Uông Bí, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0347844,
    "Latitude": 106.769557
  },
  {
    "STT": 181,
    "Name": "\tCơ Sở Bán thuốc Đông Y, thuốc Từ Dược Liệu\t",
    "address": "\tThôn 5, xã Quảng Chính, huyện Hải Hà, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.4563555,
    "Latitude": 107.7060389
  },
  {
    "STT": 182,
    "Name": "\tCơ Sở thuốc Đông Y Thiên Thảo Đường\t",
    "address": "\tTổ 30D khu 2, phường Cao Xanh, Tphường Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9722852,
    "Latitude": 107.0834734
  },
  {
    "STT": 183,
    "Name": "\tNhà thuốc Ngọc Hoa\t",
    "address": "\tKi ốt số 10 chợ Cẩm Thành, phường Cẩm Thành, TP Cẩm Phả, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0125041,
    "Latitude": 107.279108
  },
  {
    "STT": 184,
    "Name": "\tNhà thuốc Hà Thu\t",
    "address": "\tSố nhà 237, đường Cao Thắng, phường Cao Thắng, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9608936,
    "Latitude": 107.0925003
  },
  {
    "STT": 185,
    "Name": "\tQuầy thuốc Số 5 - Công Ty Cổ Phần Dược Vật Tư Y Tế Quảng Ninh\t",
    "address": "\tKi ốt chợ Sa Tô, phường Cao Xanh, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9777619,
    "Latitude": 107.0872968
  },
  {
    "STT": 186,
    "Name": "\tQuầy thuốc Số 14 - Công Ty Cổ Phần Dược Vật Tư Y Tế Quảng Ninh\t",
    "address": "\tTổ 6 khu 8, phường Hồng Hà, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9499748,
    "Latitude": 107.1041465
  },
  {
    "STT": 187,
    "Name": "\tQuầy thuốc Số 8 - Công Ty Cổ Phần Dược Vật Tư Y Tế Quảng Ninh\t",
    "address": "\tSố 12 ,  đường Trần Phú, phường Trần Phú, thành phố Móng Cái, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.5353709,
    "Latitude": 107.9724429
  },
  {
    "STT": 188,
    "Name": "\tQuầy thuốc Số 35 - Công Ty Cổ Phần Dược Vật Tư Y Tế Quảng Ninh\t",
    "address": "\tSố 711 ,  đường An Tiêm, phường Hà Khẩu, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9860989,
    "Latitude": 106.9962519
  },
  {
    "STT": 189,
    "Name": "\tQuầy thuốc Doanh Nghiệp Số 46 - Công Ty TNHH Dược Phẩm Bạch Đằng\t",
    "address": "\tThôn 1, xã Hạ Long, huyệnVân Đồn, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0981538,
    "Latitude": 107.4553657
  },
  {
    "STT": 190,
    "Name": "\tthuốc Đông Y Đại Lợi\t",
    "address": "\tSố nhà 90, tổ 15 khu 1, phường Bạch Đằng, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9533239,
    "Latitude": 107.0871119
  },
  {
    "STT": 191,
    "Name": "\tQuầy thuốc Doanh Nghiệp Số 8 - Công Ty Cổ Phần Thương Mại Và Dược Phẩm Nam Việt\t",
    "address": "\tXã Nguyễn Huệ, huyện Đông Triều, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0958153,
    "Latitude": 106.6055534
  },
  {
    "STT": 192,
    "Name": "\tQuầy thuốc Doanh Nghiệp Số 9 - Công Ty Cổ Phần Thương Mại Và Dược Phẩm Nam Việt\t",
    "address": "\tThôn Mễ Xá 1, xã Hưng Đạo, huyện Đông Triều, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0661883,
    "Latitude": 106.5260001
  },
  {
    "STT": 193,
    "Name": "\tQuầy thuốc Doanh Nghiệp Số 22 - Công Ty TNHH Dược Phẩm Hồng Dương\t",
    "address": "\tThôn Vườn Cau, xã Sơn Dương, huyện Hoành Bồ, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.067638,
    "Latitude": 106.9764447
  },
  {
    "STT": 194,
    "Name": "\tQuầy thuốc Doanh Nghiệp Số 103 - Công Ty TNHH Dược Phẩm Hồng Dương\t",
    "address": "\tThôn Mễ Xá, xã Hưng Đạo, huyện Đông Triều, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.064797,
    "Latitude": 106.5268278
  },
  {
    "STT": 195,
    "Name": "\tQuầy thuốc Khe Sím - Công Ty TNHH Dược Phẩm Hồng Dương\t",
    "address": "\tThôn Khe Sím, xã Dương Huy, thành phố Cẩm Phả, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0216205,
    "Latitude": 107.2731686
  },
  {
    "STT": 196,
    "Name": "\tNhà thuốc Tâm Phúc\t",
    "address": "\tSố 17, tổ 20 khu 3, phường Trưng Vương, thành phố Uông Bí, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.098618,
    "Latitude": 106.7927658
  },
  {
    "STT": 197,
    "Name": "\tNhà thuốc Liên Long - Công Ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tSố nhà 9, tổ 6 khu 2, phường Hồng Hà, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9579074,
    "Latitude": 107.0226806
  },
  {
    "STT": 198,
    "Name": "\tNhà thuốc Doanh Nghiệp Số 11- Công Ty TNHH Dược Phẩm Bạch Đằng\t",
    "address": "\tSố 32, tổ 2 khu 2, phường Hồng Hải, Tphường  Hạ Long, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9579074,
    "Latitude": 107.0226806
  },
  {
    "STT": 199,
    "Name": "\tNhà thuốc Ds Phạm Thị Luồng\t",
    "address": "\tSố 59 Nguyễn Du, phường Quang Trung, thành phố Uông Bí, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0347397,
    "Latitude": 106.7703621
  },
  {
    "STT": 200,
    "Name": "\tQuầy thuốc Doanh Nghiệp 115 - Công Ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tThôn Đông Hợp, xã Đông Xá, huyện Vân Đồn, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0699039,
    "Latitude": 107.4138469
  },
  {
    "STT": 201,
    "Name": "\tNhà thuốc Thiện Phúc\t",
    "address": "\tSố 293, tổ 31C khu 3, phường Cao Xanh, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9722852,
    "Latitude": 107.0834734
  },
  {
    "STT": 202,
    "Name": "\tNhà thuốc 66\t",
    "address": "\tSố nhà 493, tổ 5 khu 4, phường Hà Lầm, thành phố Hạ Long, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.970539,
    "Latitude": 107.1144837
  },
  {
    "STT": 203,
    "Name": "\tNhà thuốc Số 2 - Công Ty TNHH Dược Phẩm Hồng Dương\t",
    "address": "\tSố 289 ,  Đường Nguyễn Văn Cừ, phường Hồng Hải, thành phố Hạ Long, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9557688,
    "Latitude": 107.0980769
  },
  {
    "STT": 204,
    "Name": "\tNhà thuốc Hợi Liên\t",
    "address": "\tTổ 39 khu 4, phường Hà Trung, thành phố Hạ Long, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9648457,
    "Latitude": 107.1277751
  },
  {
    "STT": 205,
    "Name": "\tQuầy thuốc Số 81 - Công Ty Cổ Phần Dược Vtyt Quảng Ninh\t",
    "address": "\tKi ốt chợ Cẩm Đông, phường cẩm Đông, thành phố Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0081551,
    "Latitude": 107.2919065
  },
  {
    "STT": 206,
    "Name": "\tQuầy thuốc Số 110 - Công Ty Cổ Phần Dược Vtyt Quảng Ninh\t",
    "address": "\tSố 4, phố Trần Bình Trọng, thị trấn Quảng Hà, huyện Hải Hà, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.452286,
    "Latitude": 107.7521534
  },
  {
    "STT": 207,
    "Name": "\tNhà thuốc Số 2 - Bệnh Viện Đa Khoa Tỉnh Quảng Ninh\t",
    "address": "\tTầng 1 nhà D khu khám bệnh theo yêu cầu BVĐK tỉnh ,  phường Bạch Đằng, Tphường Hạ Long, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9519114,
    "Latitude": 107.0819592
  },
  {
    "STT": 208,
    "Name": "\tNhà thuốc Bảo Tín\t",
    "address": "\tSố 115,  Đường Tuệ Tĩnh, phường  Thanh Sơn, Tphường  Uông Bí, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0374998,
    "Latitude": 106.7516414
  },
  {
    "STT": 209,
    "Name": "\tNhà thuốc Quốc Sang\t",
    "address": "\tSố 131,  Đường Trần Nhân Tông, phường  Yên Thanh, Tphường  Uông Bí, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0344942,
    "Latitude": 106.7517934
  },
  {
    "STT": 210,
    "Name": "\tNhà thuốc Tâm Đức\t",
    "address": "\tSố 167, tổ 4 khu Tây Sơn 1, phường  Cẩm Sơn, Tphường  Cẩm Phả, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0092731,
    "Latitude": 107.3109745
  },
  {
    "STT": 211,
    "Name": "\tNhà thuốc Vinh Huệ\t",
    "address": "\tTổ 4, khu Diêm Thủy, phường Cẩm Bình, thành phố Cẩm Phả, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0002112,
    "Latitude": 107.2873282
  },
  {
    "STT": 212,
    "Name": "\tNhà thuốc Khánh Ngọc\t",
    "address": "\tSố nhà 225, T8K6, phường Giếng Đáy, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9815272,
    "Latitude": 107.0214647
  },
  {
    "STT": 213,
    "Name": "\tQuầy thuốc Doanh Nghiệp Phương Vy - Công Ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tTổ 1, khu Vĩnh Tuy 1, thị trấn Mạo Khê, huyện Đông Triều, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0584203,
    "Latitude": 106.5985501
  },
  {
    "STT": 214,
    "Name": "\tQuầy thuốc Doanh Nghiệp Đức Luân - Công Ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tThôn Kim Sen, xã Kim Sơn, huyện Đông Triều, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0778256,
    "Latitude": 106.5687195
  },
  {
    "STT": 215,
    "Name": "\tQuầy thuốc Doanh Nghiệp Hương Lộc- Công Ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tThôn Làng, xã Thống Nhất, huyện Hoành Bồ, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0313655,
    "Latitude": 107.1024181
  },
  {
    "STT": 216,
    "Name": "\tQuầy thuốc Doanh Nghiệp Quỳnh Hoa - Công Ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tThôn An Biên 1, xã Lê Lợi, huyện Hoành Bồ, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0156018,
    "Latitude": 107.0063023
  },
  {
    "STT": 217,
    "Name": "\tQuầy thuốc Doanh Nghiệp Số 22 - Công Ty TNHH Dược Phẩm Bạch Đằng\t",
    "address": "\tThôn Chợ, xã Thống Nhất, huyện Hoành Bồ, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0351254,
    "Latitude": 107.0893798
  },
  {
    "STT": 218,
    "Name": "\tQuầy thuốc Doanh Nghiệp Số 6 - Công Ty TNHH Dược Phẩm Bạch Đằng\t",
    "address": "\tSN 162,  Đường Hoàng Hoa Thám, thị trấn  Mạo Khê, huyện  Đông Triều, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0584203,
    "Latitude": 106.5985501
  },
  {
    "STT": 219,
    "Name": "\tQuầy thuốc Doanh Nghiệp Số 42 - Công Ty TNHH Dược Phẩm Bạch Đằng\t",
    "address": "\tSố 53 ,  Đường Nguyễn Trãi, thị trấn Trới, huyện Hoành Bồ, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0375125,
    "Latitude": 106.9937795
  },
  {
    "STT": 220,
    "Name": "\tQuầy thuốc Doanh Nghiệp Số 51 - Công Ty TNHH Dược Phẩm Bạch Đằng\t",
    "address": "\tKhu 3, phường Hà An, thị xã Quảng Yên, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9241324,
    "Latitude": 106.8583328
  },
  {
    "STT": 221,
    "Name": "\tQuầy thuốc Doanh Nghiệp Số 40 - Công Ty TNHH Dược Phẩm Bạch Đằng\t",
    "address": "\tKhu 5, phường Phong Cốc, thị xã Quảng Yên, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.8776718,
    "Latitude": 106.7647475
  },
  {
    "STT": 222,
    "Name": "\tQuầy thuốc Doanh Nghiệp Số 52 - Công Ty TNHH Dược Phẩm Bạch Đằng\t",
    "address": "\tKhu Lâm Sinh II, phường Minh Thành, thị xã Quảng Yên, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0073587,
    "Latitude": 106.8558034
  },
  {
    "STT": 223,
    "Name": "\tQuầy thuốc Doanh Nghiệp Số 55 - Công Ty TNHH Dược Phẩm Bạch Đằng\t",
    "address": "\tThôn 8, xã Hạ Long, huyện Vân Đồn, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0623461,
    "Latitude": 107.4913489
  },
  {
    "STT": 224,
    "Name": "\tQuầy thuốc Doanh Nghiệp Số 5 - Công Ty Cổ Phần Dược Phẩm Thành Đức\t",
    "address": "\tSN 210,  Đường Nguyễn Đức Cảnh, phường  Quang Hanh, Tphường  Cẩm Phả, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0072997,
    "Latitude": 107.2255782
  },
  {
    "STT": 225,
    "Name": "\tQuầy thuốc Doanh Nghiệp Số 2 Công Dung - Công Ty TNHH Dược Phẩm Thương Mại Thái Ngọc\t",
    "address": "\tXuân Viên 3, xã Xuân Sơn, huyện Đông Triều, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0777755,
    "Latitude": 106.5488153
  },
  {
    "STT": 226,
    "Name": "\tQuầy thuốc Doanh Nghiệp Số 3 - Công Ty TNHH Dược Phẩm Thương Mại Thái Ngọc\t",
    "address": "\tThôn Vĩnh Thái, xã Hồng Thái Đông, huyện Đông Triều, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0475916,
    "Latitude": 106.6818326
  },
  {
    "STT": 227,
    "Name": "\tQuầy thuốc Số 52 - Công Ty Cổ Phần Dược Vật Tư Y Tế Quảng Ninh\t",
    "address": "\tTổ 1, khu Nam Trung, phường Nam Khê, thành phố Uông Bí, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0232141,
    "Latitude": 106.8121334
  },
  {
    "STT": 228,
    "Name": "\tCơ Sở Kinh Doanh thuốc Đông Y, thuốc Từ Dược Liệu\t",
    "address": "\tKhu 2, phường Phong Hải, thị xã Quảng Yên, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9089731,
    "Latitude": 106.8266873
  },
  {
    "STT": 229,
    "Name": "\tQuầy thuốc Doanh Nghiệp Chung Hiên - Công Ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tThôn Thượng Thông, xã Hồng Thái Đông, huyện Đông Triều, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0475916,
    "Latitude": 106.6818326
  },
  {
    "STT": 230,
    "Name": "\tQuầy thuốc Số 79 - Công Ty TNHH Dược Phẩm Hồng Dương\t",
    "address": "\tSN 150,  Hoàng Hoa Thám, thị trấn  Mạo Khê, huyện  Đông Triều, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0584203,
    "Latitude": 106.5985501
  },
  {
    "STT": 231,
    "Name": "\tNhà thuốc Bệnh Viện Bãi Cháy\t",
    "address": "\tBệnh viện Bãi Cháy, phường Giếng Đáy, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9761578,
    "Latitude": 107.0143062
  },
  {
    "STT": 232,
    "Name": "\tQuầy thuốc Doanh Nghiệp Tùng Khang - Công Ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tThôn Đồng Mô, xã Hoành Mô, huyện Bình Liêu, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.5963466,
    "Latitude": 107.4883052
  },
  {
    "STT": 233,
    "Name": "\tHộ Kinh Doanh thuốc Y Học Cổ Truyền Trường Mai\t",
    "address": "\tTổ 3 khu 5A, phường Cẩm Trung, Tphường Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0099574,
    "Latitude": 107.2749255
  },
  {
    "STT": 234,
    "Name": "\tQuầy thuốc Số 13 - Công Ty Cp Dược Vtyt Quảng Ninh\t",
    "address": "\tKhu 7, Km 3, phường Hải Yên, Tphường Móng Cái, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.5285555,
    "Latitude": 107.9319352
  },
  {
    "STT": 235,
    "Name": "\tQuầy thuốc Số 25 - Công Ty Cổ Phần Dược Vật Tư Y Tế Quảng Ninh\t",
    "address": "\tTổ 7 khu 1, phường Mông Dương, thành phố Cẩm Phả, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.055278,
    "Latitude": 107.343056
  },
  {
    "STT": 236,
    "Name": "\tQuầy thuốc Số 66 - Công Ty Cổ Phần Dược Vật Tư Y Tế Quảng Ninh\t",
    "address": "\tSố 292 đường An Tiêm, phường Hà Khẩu, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9860989,
    "Latitude": 106.9962519
  },
  {
    "STT": 237,
    "Name": "\tQuầy thuốc Số 17 - Công Ty Cổ Phần Dược Vật Tư Y Tế Quảng Ninh\t",
    "address": "\tTổ 54, khu 5, phường Hà Khẩu, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9722997,
    "Latitude": 106.9998899
  },
  {
    "STT": 238,
    "Name": "\tQuầy thuốc Số 19 - Công Ty Cổ Phần Dược Vật Tư Y Tế Quảng Ninh\t",
    "address": "\tSố 245 ,  đường An Tiêm, phường Hà Khẩu, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9860989,
    "Latitude": 106.9962519
  },
  {
    "STT": 239,
    "Name": "\tQuầy thuốc Số 20 - Công Ty Cổ Phần Dược Vật Tư Y Tế Quảng Ninh\t",
    "address": "\tSố 234 ,  đường Vũ Văn Hiếu, phường Hà Tu, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9585581,
    "Latitude": 107.1526733
  },
  {
    "STT": 240,
    "Name": "\tQuầy thuốc Số 113 - Công Ty Cổ Phần Dược Vật Tư Y Tế Quảng Ninh\t",
    "address": "\tSố 841 ,  đường Trần Phú, phường Cẩm Thủy, thành phố Cẩm Phả, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.010141,
    "Latitude": 107.2589275
  },
  {
    "STT": 241,
    "Name": "\tQuầy thuốc Số 37 - Công Ty Cổ Phần Dược Vật Tư Y Tế Quảng Ninh\t",
    "address": "\tSố 106 ,  đường Lý Bôn, phường Cẩm Đông, thành phố Cẩm Phả, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0081551,
    "Latitude": 107.2919065
  },
  {
    "STT": 242,
    "Name": "\tNhà thuốc Doanh Nghiệp Hải Linh - Công Ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tTổ 7 khu Tre Mai, phường Nam Khê, thành phố Uông Bí, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0265928,
    "Latitude": 106.8065339
  },
  {
    "STT": 243,
    "Name": "\tQuầy thuốc Dn Mai Hiên - Công Ty TNHHdp Hạ Long\t",
    "address": "\tSố 132 tổ 1 khu Công Nông, phường Mạo Khê, TX. Đông Triều, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0675098,
    "Latitude": 106.5996595
  },
  {
    "STT": 244,
    "Name": "\tQuầy thuốc Doanh Nghiệp Ngọc Lan - Công Ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tĐội 2, thôn Kim Sen, xã Kim Sơn, huyện Đông Triều, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0719973,
    "Latitude": 106.5686164
  },
  {
    "STT": 245,
    "Name": "\tNhà thuốc Chu Kim Chung\t",
    "address": "\tSố nhà 603,  Đường Nguyễn văn Cừ, phường Hồng Hà, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9452755,
    "Latitude": 107.1243771
  },
  {
    "STT": 246,
    "Name": "\tNhà thuốc Ngân Sơn\t",
    "address": "\tSố nhà 87, tổ 2 khu 5, phường Giếng Đáy, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9815272,
    "Latitude": 107.0214647
  },
  {
    "STT": 247,
    "Name": "\tQuầy thuốc Doanh Nghiệp Hương Lý- Công Ty TNHH Dp Hạ Long\t",
    "address": "\tSố 16 phố Thống Nhất, thị trấn Tiên Yên, huyện Tiên Yên, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.3289859,
    "Latitude": 107.3966896
  },
  {
    "STT": 248,
    "Name": "\tNhà thuốc Trọng Vinh\t",
    "address": "\tTổ 9 khu 2 phường Bãi Cháy, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9593434,
    "Latitude": 107.0227511
  },
  {
    "STT": 249,
    "Name": "\tQuầy thuốc Hải Thủy- Công Ty TNHH Dp Hồng Dương\t",
    "address": "\tSố 118 ,  Nguyễn Bình, thị trấn Đông Triều, huyện Đông Triều, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.087021,
    "Latitude": 106.516197
  },
  {
    "STT": 250,
    "Name": "\tQuầy thuốc Khánh Ngọc- Công Ty TNHH Dp Hồng Dương\t",
    "address": "\tThôn Mễ Xá, xã Hưng Đạo, huyện Đông Triều, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.064797,
    "Latitude": 106.5268278
  },
  {
    "STT": 251,
    "Name": "\tQuầy thuốc Huyền Trang- Công Ty TNHH Dp Hồng Dương\t",
    "address": "\tThôn 11 xã Hải Đông, thành phố Móng Cái, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.5394395,
    "Latitude": 107.8835642
  },
  {
    "STT": 252,
    "Name": "\tQuầy thuốc Doanh Nghiệp Huấn Oanh - Công Ty TNHH Dp Hồng Dương\t",
    "address": "\tThôn Mễ Xá, xã Hưng Đạo, huyện Đông Triều, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.064797,
    "Latitude": 106.5268278
  },
  {
    "STT": 253,
    "Name": "\tQuầy thuốc Doanh Nghiệp Hải Chi - Công Ty TNHH Dp Hồng Dương\t",
    "address": "\tKhu 2, thị trấn Ba Chẽ, huyện Ba Chẽ, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.2718654,
    "Latitude": 107.2836989
  },
  {
    "STT": 254,
    "Name": "\tQuầy thuốc Doanh Nghiệp Số 8 - Công Ty TNHH Dp Hồng Dương\t",
    "address": "\tPhố Đông Tiến, thị trấn Tiên Yên, huyện Tiên yên, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.3347973,
    "Latitude": 107.3937536
  },
  {
    "STT": 255,
    "Name": "\tQuầy thuốc Số 21 - Công Ty Cổ Phần Dược Vật Tư Y Tế Quảng Ninh\t",
    "address": "\tSố 90 ,  đường Cao Thắng, phường Trần Hưng Đạo, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9570425,
    "Latitude": 107.0856728
  },
  {
    "STT": 256,
    "Name": "\tQuầy thuốc Số 38 - Công Ty Cổ Phần Dược Vật Tư Y Tế Quảng Ninh\t",
    "address": "\tTổ 64 khu Bình Minh, phường Cẩm Bình, thành phố Cẩm Phả, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0006682,
    "Latitude": 107.2868586
  },
  {
    "STT": 257,
    "Name": "\tQuầy thuốc Số 6 - Công Ty Cổ Phần Dược Vật Tư Y Tế Quảng Ninh\t",
    "address": "\tSố 42 ,  đường Trần Phú, phường Trần Phú, thành phố Móng Cái, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.532385,
    "Latitude": 107.9713391
  },
  {
    "STT": 258,
    "Name": "\tQuầy thuốc Số 34 - Công Ty Cổ Phần Dược Vật Tư Y Tế Quảng Ninh\t",
    "address": "\tSố 214 ,  đường Đồng Đăng, phường Việt Hưng, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9955245,
    "Latitude": 106.9670788
  },
  {
    "STT": 259,
    "Name": "\tQuầy thuốc Số 45 - Công Ty Cổ Phần Dược Vật Tư Y Tế Quảng Ninh\t",
    "address": "\tSố 251 ,  đường Đồng Đăng, phường Việt Hưng, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.9614339,
    "Latitude": 106.702825
  },
  {
    "STT": 260,
    "Name": "\tQuầy thuốc Số 50 - Công Ty Cổ Phần Dược Vật Tư Y Tế Quảng Ninh\t",
    "address": "\tSố 222, tổ 8A khu 3, phường Hùng Thắng, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9576774,
    "Latitude": 107.0007987
  },
  {
    "STT": 261,
    "Name": "\tQuầy thuốc Số 133 - Công Ty Cổ Phần Dược Vật Tư Y Tế Quảng Ninh\t",
    "address": "\tThôn 1, xã Đức Chính, huyện Đông Triều, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0797764,
    "Latitude": 106.5145667
  },
  {
    "STT": 262,
    "Name": "\tQuầy thuốc Số 132 - Công Ty Cổ Phần Dược Vật Tư Y Tế Quảng Ninh\t",
    "address": "\tKi ốt chợ Cột Đông Triều, huyện Đông Triều, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0834047,
    "Latitude": 106.5127441
  },
  {
    "STT": 263,
    "Name": "\tQuầy thuốc Doanh Nghiệp Số 133 - Công Ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tThôn 1, xã Tiến Tới, huyện Hải Hà, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.4035334,
    "Latitude": 107.665927
  },
  {
    "STT": 264,
    "Name": "\tQuầy thuốc Hải Bình- Công Ty TNHH Dược Phẩm Hồng Dương\t",
    "address": "\tThôn Mễ Xá, xã Hưng Đạo, huyện Đông Triều, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.064797,
    "Latitude": 106.5268278
  },
  {
    "STT": 265,
    "Name": "\tQuầy thuốc Doanh Nghiệp Số 11 - Công Ty Cổ Phần Thương Mại Và Dược Phẩm Nam Việt\t",
    "address": "\tXóm Sen, xã Tiền An, thị xã Quảng Yên, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9334638,
    "Latitude": 106.8414374
  },
  {
    "STT": 266,
    "Name": "\tQuầy thuốc Thu Hoài - Công Ty TNHH Dược Phẩm Hồng Dương\t",
    "address": "\tThôn Nhuệ Hổ, xã Kim Sơn, huyện Đông Triều, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.070062,
    "Latitude": 106.5553799
  },
  {
    "STT": 267,
    "Name": "\tQuầy thuốc Doanh Nghiệp Gia Chi - Công Ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tSố nhà 111, tổ 3 khu 10, thị trấnTrới, huyện Hoành Bồ, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0551849,
    "Latitude": 107.0405167
  },
  {
    "STT": 268,
    "Name": "\tCông ty Cổ phần Dược phẩm Sunli\t",
    "address": "\tKm 3 khu 7, phường Hải Yên, thành phố Móng Cái, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.5244675,
    "Latitude": 107.91574
  },
  {
    "STT": 269,
    "Name": "\tNhà thuốc Hà Thơ - Công ty Cổ phần Dược phẩm Sunli\t",
    "address": "\tKhu 7, phường Hải Yên, thành Phố Móng Cái, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.534966,
    "Latitude": 107.948827
  },
  {
    "STT": 270,
    "Name": "\tQuầy thuốc DN số 54- Cty TNHHDP Bạch Đằng\t",
    "address": "\tThôn Vĩnh Thái, xã Hồng Thái Đông, huyện Đông Triều, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0475916,
    "Latitude": 106.6818326
  },
  {
    "STT": 271,
    "Name": "\tQuầy thuốc DN số 54- Cty TNHHDP Bạch Đằng\t",
    "address": "\tThôn Vĩnh Thái, xã Hồng Thái Đông, huyện Đông Triều, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0475916,
    "Latitude": 106.6818326
  },
  {
    "STT": 272,
    "Name": "\tQuầy thuốc DN số 43 - Cty TNHHDP Bạch Đằng\t",
    "address": "\tThôn Xuân Quang, xã Yên Thọ, huyện Đông Triều, Q.Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0567778,
    "Latitude": 106.6202887
  },
  {
    "STT": 273,
    "Name": "\tQuầy thuốc DN số 4 - Cty Công ty CP Dược VTYT Quảng Ninh\t",
    "address": "\tKi ốt số 121, chợ Hồng Hà, phường Hồng Hà, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9479461,
    "Latitude": 107.1295067
  },
  {
    "STT": 274,
    "Name": "\tQuầy thuốc DN số 11 - Cty Công ty CP Dược VTYT Quảng Ninh\t",
    "address": "\tTổ 37 khu 4, phường Cao Thắng, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.966594,
    "Latitude": 107.096763
  },
  {
    "STT": 275,
    "Name": "\tCơ Sở Hương Giang\t",
    "address": "\tKi ốt số 7 Chợ Cẩm Đông, phường Cẩm Đông, thành phố Cẩm Phả, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0081551,
    "Latitude": 107.2919065
  },
  {
    "STT": 276,
    "Name": "\tTrần Thị Len\t",
    "address": "\tKi ốt số 26 chợ Cẩm Đông, phường Cẩm Đông, thành phố cẩm Phả, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0081551,
    "Latitude": 107.2919065
  },
  {
    "STT": 277,
    "Name": "\tCơ Sở Phúc Trường\t",
    "address": "\tKi ốt số 178 +165 tầng trệt chợ Hồng Hà, phường Hồng Hà, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9479461,
    "Latitude": 107.1295067
  },
  {
    "STT": 278,
    "Name": "\tNhà Thuốc Tuấn Thủy - Công Ty Cổ phần Thương Mại Và Dược Phẩm Nam Việt\t",
    "address": "\tSố nhà 38, đường Minh Hà, phường Hà Tu, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9565441,
    "Latitude": 107.1608681
  },
  {
    "STT": 279,
    "Name": "\tCông ty Cổ phần Dược phẩm Thành Đức\t",
    "address": "\tSố nhà 384 ,  Đường Trần Phú, phường Cẩm Trung, thành phố Cẩm Phả, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0097891,
    "Latitude": 107.270649
  },
  {
    "STT": 280,
    "Name": "\tNhà thuốc Thúy Hằng\t",
    "address": "\tSố nhà 103, tổ 2 khu 4, phường Giếng Đáy, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9815272,
    "Latitude": 107.0214647
  },
  {
    "STT": 281,
    "Name": "\tNhà thuốc Hùng Loan\t",
    "address": "\tKhu Biểu Nghi, phường Đông Mai, thị xã Quảng Yên, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9980414,
    "Latitude": 106.8440285
  },
  {
    "STT": 282,
    "Name": "\tNhà thuốc Minh Trang\t",
    "address": "\tSố nhà 438, tổ 2 khu 6B, phường Hồng Hải, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9499748,
    "Latitude": 107.1041465
  },
  {
    "STT": 283,
    "Name": "\tNhà thuốc Lâm Phương - Công ty CP Dược VTYT Q.Ninh\t",
    "address": "\tSố nhà 283 ,  Đường Cao Xanh, Phường Cao Xanh, Thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9760789,
    "Latitude": 107.074614
  },
  {
    "STT": 284,
    "Name": "\tNhà thuốc số 1- Công ty Cp thương mại và Dược phẩm Nam Việt\t",
    "address": "\tSố nhà 234,  Đường Nguyễn Văn Cừ, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9557688,
    "Latitude": 107.0980769
  },
  {
    "STT": 285,
    "Name": "\tNhà thuốc Hùng Mạnh 2 - Công ty CP Dược VTYT Q.Ninh\t",
    "address": "\tSố nhà 574, phố Trần Nhân Tông, phường Thanh Sơn, thành phố Uông Bí, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0344857,
    "Latitude": 106.7522579
  },
  {
    "STT": 286,
    "Name": "\tNhà thuốc doanh nghiệp Tân An - Cty TNHH DP Bạch Đằng\t",
    "address": "\tKi ốt 27+28 chợ Trung Tâm Mạo Khê, thị trấn Mạo Khê, huyện Đông Triều, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0584203,
    "Latitude": 106.5985501
  },
  {
    "STT": 287,
    "Name": "\tQuầy thuốc số 9 - Công Ty CP Dược VTYT Q.Ninh\t",
    "address": "\tSố nhà 113, tổ 1 khu 1, Phường Hà Trung, Thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9533239,
    "Latitude": 107.0871119
  },
  {
    "STT": 288,
    "Name": "\tQuầy thuốc doanh nghiệp số 41 - Công ty TNHH DP Hạ Long\t",
    "address": "\tThôn 1 xã Hồng Thái Tây, huyện Đông Triều, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0500496,
    "Latitude": 106.6586052
  },
  {
    "STT": 289,
    "Name": "\tQuầy thuốc doanh nghiệp số 58 - Công ty TNHH DP Hạ Long\t",
    "address": "\tSố nhà 168 ,  tổ 2 khu Cầu Trắng, phường Đại Yên, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9510629,
    "Latitude": 107.1350169
  },
  {
    "STT": 290,
    "Name": "\tQuầy thuốc doanh nghiệp Ánh Tuyết- Công ty TNHH DP Hạ Long\t",
    "address": "\tTổ 4 khu Quỳnh Trung, phường Đại Yên, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.983019,
    "Latitude": 106.9078245
  },
  {
    "STT": 291,
    "Name": "\tQuầy thuốc doanh nghiệp Thảo vân- Công ty TNHH DP Hạ Long\t",
    "address": "\tThôn Tràng Bạch, Xã Hoàng Quế, huyện Đông Triều, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0549597,
    "Latitude": 106.642214
  },
  {
    "STT": 292,
    "Name": "\tQuầy thuốc doanh nghiệp Hoa Hồng- Công ty TNHH DP Hạ Long\t",
    "address": "\tSố nhà 83 ,  phố 1, thị trấn Mạo Khê, huyện Đông Triều, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.058394,
    "Latitude": 106.5934706
  },
  {
    "STT": 293,
    "Name": "\tQuầy thuốc doanh nghiệp số 125- Công ty TNHH DP Hạ Long\t",
    "address": "\tThôn Xuân Bình, xã Bình Khê, huyện Đông Triều, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.1054044,
    "Latitude": 106.5854143
  },
  {
    "STT": 294,
    "Name": "\tQuầy thuốc doanh nghiệp Việt Dung- Công ty TNHH DP Hạ Long\t",
    "address": "\tKhu Bình Quyền, thị trấn Bình Liêu, huyện Bình Liêu, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.5248107,
    "Latitude": 107.4033045
  },
  {
    "STT": 295,
    "Name": "\tQuầy thuốc doanh nghiệp Hùng Hường- Công ty TNHH DP Hạ Long\t",
    "address": "\tSố nhà 99, khu Vĩnh Hòa, Thị trấn Mạo Khê, huyện Đông Triều, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0598621,
    "Latitude": 106.5869951
  },
  {
    "STT": 296,
    "Name": "\tQuầy thuốc doanh nghiệp số 118- Công ty TNHH DP Hạ Long\t",
    "address": "\tKi ốt số 189 chợ Đầm Hà, thị trấn Đầm Hà, huyện Đầm Hà, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.3500114,
    "Latitude": 107.5992193
  },
  {
    "STT": 297,
    "Name": "\tNhà thuốc Hồng Huy\t",
    "address": "\tTổ 8 khu Lạc Thanh, phường Yên Thanh, Tphường  Uông Bí, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0184158,
    "Latitude": 106.7541075
  },
  {
    "STT": 298,
    "Name": "\tNhà thuốc Bệnh Viện Đa Khoa Khu Vực Tiên Yên\t",
    "address": "\tBệnh viện ĐKKV Tiên Yên, phố Lý Thường Kiệt, Thị Trấn Tiên Yên, huyện Tiên Yên, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.3325573,
    "Latitude": 107.4019703
  },
  {
    "STT": 299,
    "Name": "\tCông Ty Tnhh Dược Phẩm Thương Mại Đức Dung\t",
    "address": "\tSố nhà 32 lô B4 khu đô thị Bãi Muối, phường Cao Thắng, thành phố Hạ Long, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9631743,
    "Latitude": 107.100852
  },
  {
    "STT": 300,
    "Name": "\tQuầy thuốc Doanh Nghiệp Số 45 - Công Ty Tnhh Dược Phẩm Bạch Đằng\t",
    "address": "\tSố 169 Lý Thường Kiệt, thị trấn Tiên Yên, huyện Tiên Yên, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.3301782,
    "Latitude": 107.398579
  },
  {
    "STT": 301,
    "Name": "\tNhà thuốc Tuấn Thủy - Công Ty Cổ Phần Thương Mại Và Dược Phẩm Nam Việt\t",
    "address": "\tSố 359 Đường Vũ Văn Hiếu, phường Hà Tu, thành phố Hạ Long, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9585581,
    "Latitude": 107.1526733
  },
  {
    "STT": 302,
    "Name": "\tQuầy thuốc Doanh Nghiệp Tiến Thành - Công Ty Cổ Phần Thương Mại Và Dược Phẩm Nam Việt\t",
    "address": "\tTổ 6 khu Vĩnh Tuy 2, thị trấn Mạo Khê, huyện Đông Triều, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0455727,
    "Latitude": 106.5968968
  },
  {
    "STT": 303,
    "Name": "\tQuầy thuốc Doanh Nghiệp Khánh Tâm - Công Ty Tnhh Dược Phẩm Hạ Long\t",
    "address": "\tThôn Bình Lục Thượng, xã Hồng Phong, huyện Đông Triều, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0743131,
    "Latitude": 106.5024344
  },
  {
    "STT": 304,
    "Name": "\tQuầy thuốc Doanh Nghiệp Số 12 - Công Ty Cổ Phần Thương Mại Và Dược Phẩm Nam Việt\t",
    "address": "\tXóm 3, xã Hiệp Hòa, thị xã Quảng Yên, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9541241,
    "Latitude": 106.7820375
  },
  {
    "STT": 305,
    "Name": "\tQuầy thuốc Doanh Nghiệp Khánh Hà- Công Ty Cổ Phần Thương Mại Và Dược Phẩm Nam Việt\t",
    "address": "\tThôn Tân Lập, xã Tân Yên, huyện Đông Triều , tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0354236,
    "Latitude": 106.6748181
  },
  {
    "STT": 306,
    "Name": "\tNhà thuốc Hoài Nam\t",
    "address": "\tSố 367 Đường Trần Phú, phường Cẩm Thành, thành phố Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0118793,
    "Latitude": 107.2790011
  },
  {
    "STT": 307,
    "Name": "\tQuầy thuốc Doanh Nghiệp Quảng La - Công Ty Tnhh Dược Phẩm Hạ Long\t",
    "address": "\tThôn 5 xã Quảng La, huyện Hoành Bồ, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0866623,
    "Latitude": 106.9027877
  },
  {
    "STT": 308,
    "Name": "\tNhà thuốc Khánh Ngọc\t",
    "address": "\tSố 192, Tổ 5 Khu 3 Phường Hà Tu, Thành Phố Hạ Long, Tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9546746,
    "Latitude": 107.1425442
  },
  {
    "STT": 309,
    "Name": "\tNhà thuốc Bảo Yến\t",
    "address": "\tSố 141, Tổ 2 Khu Cao Sơn 2, Phường Cao Sơn, Thành phố Cẩm Phả, Tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0037902,
    "Latitude": 107.312133
  },
  {
    "STT": 310,
    "Name": "\tNhà thuốc Huy Hà\t",
    "address": "\tSố nhà 188, Tổ 2 Khu 5, Phường Yết Kiêu, Thành phố Hạ Long, Tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9610438,
    "Latitude": 107.0772301
  },
  {
    "STT": 311,
    "Name": "\tNhà thuốc Ngọc Diệp\t",
    "address": "\tSố 9 Tổ 1 Khu 3, Phường Hà Lầm, Thành phố Hạ Long, Tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9533239,
    "Latitude": 107.0871119
  },
  {
    "STT": 312,
    "Name": "\tQuầy thuốc số 29 - Công Ty CP Dược VTYT Q.Ninh\t",
    "address": "\tSố 10 Tổ 3 khu 6, Phường Bãi Cháy, Thành phố Hạ Long, Tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9590927,
    "Latitude": 107.0728813
  },
  {
    "STT": 313,
    "Name": "\tQuầy thuốc số 78 - Công Ty CP Dược VTYT Q.Ninh\t",
    "address": "\tKi ốt số 1 chợ Giếng Đáy, Phường Giếng Đáy, Thành phố Hạ Long, Tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9775891,
    "Latitude": 107.0084098
  },
  {
    "STT": 314,
    "Name": "\tQuầy thuốc số 124 - Công Ty CP Dược VTYT Q.Ninh\t",
    "address": "\tÔ số 5 ,  ki ốt chợ cột 3, Phường Hồng Hải, Thành phố Hạ Long, Tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9542838,
    "Latitude": 107.1021962
  },
  {
    "STT": 315,
    "Name": "\tNhà thuốc Trường Thủy\t",
    "address": "\tSố nhà 184, Tổ 3 Khu 9, Phường Bãi Cháy, Thành phố Hạ Long, Tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9643837,
    "Latitude": 107.0480378
  },
  {
    "STT": 316,
    "Name": "\tQuầy thuốc số 72 - Công Ty CP Dược VTYT Q.Ninh\t",
    "address": "\tSố 34,  Đường Tuệ Tĩnh, Phường Ka Long, Thành phố Móng Cái\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.5251109,
    "Latitude": 107.9622663
  },
  {
    "STT": 317,
    "Name": "\tCơ sở kinh doanh thuốc YHCT Kim Ngân\t",
    "address": "\tThôn Yên Trung, Xã Yên Thọ, Huyện Đông Triều, Tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0526729,
    "Latitude": 106.6220499
  },
  {
    "STT": 318,
    "Name": "\tCơ sở kinh doanh thuốc YHCT Minh Đức\t",
    "address": "\tChợ Trung Tâm Mạo Khê, Huyện Đông Triều, Tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0675098,
    "Latitude": 106.5996595
  },
  {
    "STT": 319,
    "Name": "\tCơ sở kinh doanh thuốc YHCT\t",
    "address": "\tKi ốt số 35+36,  Tầng 1 Chợ Rừng, Thị xã Quảng Yên, Tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9392012,
    "Latitude": 106.7991686
  },
  {
    "STT": 320,
    "Name": "\tCơ Sở thuốc Y học cổ truyền Minh Phắn\t",
    "address": "\tSố 31B,  Đường Hùng Vương, Thành Phố Móng Cái, Tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.5313213,
    "Latitude": 107.9591522
  },
  {
    "STT": 321,
    "Name": "\tCơ sở thuốc Y học cổ truyền Lâm Đông\t",
    "address": "\tSố 585 tầng 3 Chợ Trung Tâm, Thành phố Móng Cái, Tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.5312507,
    "Latitude": 107.9682761
  },
  {
    "STT": 322,
    "Name": "\tCơ sở thuốc Y học cổ truyền An Sinh Đường\t",
    "address": "\tSố 63 Đường Hùng Vương, Thành phố Móng Cái, Tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.5278509,
    "Latitude": 107.9703632
  },
  {
    "STT": 323,
    "Name": "\tNhà Thuốc Minh Nguyệt 2\t",
    "address": "\tSố 320, đường Trần Phú, phường Cẩm Thành, thành phố Cẩm Phả, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0118793,
    "Latitude": 107.2790011
  },
  {
    "STT": 324,
    "Name": "\tNhà Thuốc Tư Nhân Tâm An\t",
    "address": "\tSố nhà 755 Trần Phú, tổ 63 khu Tân Lập 1, phường Cẩm Thủy, thành phố Cẩm Phả, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0098631,
    "Latitude": 107.2636695
  },
  {
    "STT": 325,
    "Name": "\tNhà Thuốc Hằng Ngọc 2\t",
    "address": "\tSố 23, tổ 1 khu 1, phường Thanh Sơn, thành phố Uông Bí, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0934902,
    "Latitude": 106.7935031
  },
  {
    "STT": 326,
    "Name": "\tNhà Thuốc Phương Liên\t",
    "address": "\tSố 161, tổ 2 khu 4, phường Giếng Đáy, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9815272,
    "Latitude": 107.0214647
  },
  {
    "STT": 327,
    "Name": "\tQuầy Thuốc Tiên Lãng - Công Ty TNHH Dược Phẩm Hồng Dương\t",
    "address": "\tThôn Thác Bưởi 1, xã Tiên Lãng, huyện Tiên Yên, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.3045964,
    "Latitude": 107.4332397
  },
  {
    "STT": 328,
    "Name": "\tQuầy Thuốc Doanh Nghiệp Hiếu Phương- Công Ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tSố 77 thôn Kim Thành, xã Kim Sơn, huyện Đông Triều, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0958153,
    "Latitude": 106.6055534
  },
  {
    "STT": 329,
    "Name": "\tQuầy Thuốc Doanh Nghiệp Lê Thúy - Công Ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tSố 46 Phố Trần Bình Trọng, thị trấn Quảng Hà, huyện Hải Hà, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.452286,
    "Latitude": 107.7521534
  },
  {
    "STT": 330,
    "Name": "\tNhà thuốc Kênh Liêm\t",
    "address": "\tSố 14, tổ 1 khu 1A, phường Hồng Hải thành phố Hạ Long\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9499748,
    "Latitude": 107.1041465
  },
  {
    "STT": 331,
    "Name": "\tNhà thuốc Kim Chi\t",
    "address": "\tKi ốt số 17, chợ Hạ Long1, phường  bạch Đằng, TPHL\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9506862,
    "Latitude": 107.082389
  },
  {
    "STT": 332,
    "Name": "\tQuầy thuốc doanh nghiệp số 40- C.ty CP Dược VTYTQN\t",
    "address": "\tSố 21, phố Tre Mai, Phường Nam Khê, Uông Bí QN\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0251356,
    "Latitude": 106.808365
  },
  {
    "STT": 333,
    "Name": "\tQuầy thuốc số 16- Cty CP Dược VTYTQN\t",
    "address": "\tKhu 7, Phường Hải Yên, TP Móng Cái\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.5300965,
    "Latitude": 107.927503
  },
  {
    "STT": 334,
    "Name": "\tQuầy thuốc số 131- C .ty CP Dược VTYT QN\t",
    "address": "\tKi ốt 23,24 chợ Mạo Khê, TTr Mạo Khê, huyện  Đông Triều, QN\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0558636,
    "Latitude": 106.603251
  },
  {
    "STT": 335,
    "Name": "\tNhà thuốc số 1- Cty CP Dược VTYT QN\t",
    "address": "\t12,  Lê Quý Đôn, Hạ Long QN\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9505075,
    "Latitude": 107.0808706
  },
  {
    "STT": 336,
    "Name": "\tQuầy thuốc DN số 2- C.ty CP Dược VTYTQN\t",
    "address": "\tTổ 1 Khu 1, phường  Hà Khánh, Hạ Long, QN\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.011095,
    "Latitude": 107.129381
  },
  {
    "STT": 337,
    "Name": "\tNhà thuốc số 1B- Cty TNHHDP Bạch Đằng\t",
    "address": "\tSố 2 Đường Lê Lai, phường  Yết Kieu, Hạ Long QN\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9586794,
    "Latitude": 107.0851243
  },
  {
    "STT": 338,
    "Name": "\tQuầy thuốc doanh nghiêp số 21- Cty TNHHDP Bạch Đằng\t",
    "address": "\tTổ 11K1, phường  Quang Hanh, TP Cẩm Phả QN\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0082107,
    "Latitude": 107.2271147
  },
  {
    "STT": 339,
    "Name": "\tNhà thuốc doanh nghiệp số1- Cty TNHHDP Bạch Đằng\t",
    "address": "\tSố 425, Lê Thánh Tông, phường  Bạch Đằng, Hạ Long QN\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9514558,
    "Latitude": 107.0820234
  },
  {
    "STT": 340,
    "Name": "\tNhà thuốc doanh nghiệp số 1C- Cty TNHHDP Bạch Đằng\t",
    "address": "\tSố 80 Đường Tô Hiệu, phường  Cẩm Trung, TP Cẩm Phả QN\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0070716,
    "Latitude": 107.2699703
  },
  {
    "STT": 341,
    "Name": "\tNhà thuốc doanh nghiệp số 5- Cty TNHHDP Bạch Đằng\t",
    "address": "\tTổ 159 Khu 6, phường  Quang Trung, TP Uông Bí QN\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.1057847,
    "Latitude": 106.8075136
  },
  {
    "STT": 342,
    "Name": "\tQuầy thuốc số 129- Cty CP Dược VTYTQN\t",
    "address": "\tKi ốt 25,26 chợ Mạo Khê, TTr Mạo Khê, huyện  Đông Triều, QN\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0558636,
    "Latitude": 106.603251
  },
  {
    "STT": 343,
    "Name": "\tCông ty TNHHTM Mỹ Hoa\t",
    "address": "\tSố 30,  Tô Hiến Thành, phường  Trần Hưng Đạo. Hạ Long QN\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9576657,
    "Latitude": 107.0846437
  },
  {
    "STT": 344,
    "Name": "\tQuầy thuốc doanh nghiệp Kiên Thuận- Cty TNHHDP Hồng Dương\t",
    "address": "\tSố 563,  Tổ 2 khu 4,  TTr Cái Rồng, huyện  Vân Đồn QN\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.053875,
    "Latitude": 107.4351532
  },
  {
    "STT": 345,
    "Name": "\tQuầy thuốc số 6- Cty TNHHDP Hồng Dương\t",
    "address": "\tSố 32,  Khu Vĩnh Thông,  TTr Mạo Khê,  Đông Triều Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0568822,
    "Latitude": 106.5982832
  },
  {
    "STT": 346,
    "Name": "\tQuầy thuốc doanh nghiệp Khiêm Thủy- Cty TNHHDP Hồng Dương\t",
    "address": "\tThôn Bằng Xăm, Xã Lê Lợi, huyện  Hoành Bồ, QN\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0169087,
    "Latitude": 107.0421324
  },
  {
    "STT": 347,
    "Name": "\tNhà thuốc Hưng Hà- Công ty TNHHDP Hồng Dương\t",
    "address": "\tSố 544,  Tuệ Tĩnh, phường  Thanh Sơn Uông Bí QN\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0374998,
    "Latitude": 106.7516414
  },
  {
    "STT": 348,
    "Name": "\tQuầy thuốc Hải Tiến-Công ty TNHHDP Hồng Dương\t",
    "address": "\tThôn 8 Km 13 xã Hải Tiến, TP Móng Cái, QN\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.5394988,
    "Latitude": 107.8569058
  },
  {
    "STT": 349,
    "Name": "\tQuầy thuốc Ngân Thương- Công ty TNHHDP Hồng Dương\t",
    "address": "\tSố nhà 48, Khu 5, Phố Đầm Buôn, TTr Ba Chẽ, QN\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.294325,
    "Latitude": 107.1484521
  },
  {
    "STT": 350,
    "Name": "\tQuầy thuốc Doanh Nghiệp Linh Loan - Công TNHH Dược Phẩm Hạ Long\t",
    "address": "\tThôn Thọ Sơn, xã Yên Thọ, thị xã Đông Triều, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0608783,
    "Latitude": 106.6169661
  },
  {
    "STT": 351,
    "Name": "\tQuầy thuốcDN Hồng Nhung- C.ty CPTM và DP Nam Việt\t",
    "address": "\tSN 144, Phố Nguyễn Bình, TT Đông Triều,Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0832498,
    "Latitude": 106.511486
  },
  {
    "STT": 352,
    "Name": "\tQuầy thuốc số 1- Công ty CP Dược VTYT Quảng Ninh\t",
    "address": "\tSố 112, Đường Bãi Muối, phường Cao Thắng, TP Hạ Long\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9687487,
    "Latitude": 107.0970687
  },
  {
    "STT": 353,
    "Name": "\tCông ty CP thương mại và Dược phẩm Nam Việt\t",
    "address": "\tSố 67,  Phố Hải Lộc, phường  Hồng Hải, TPHL,QN\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9469525,
    "Latitude": 107.105489
  },
  {
    "STT": 354,
    "Name": "\tNhà Thuốc Hồng Dương 1- C.ty TNHHDP Hồng Dương\t",
    "address": "\tSố 83 tổ 4 khu 1A,phường Cao Thắng TPHL,QN\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.966594,
    "Latitude": 107.096763
  },
  {
    "STT": 355,
    "Name": "\tNhà thuốc Y cao Hà Nội\t",
    "address": "\tSố 907,  Trần Phú phường Cẩm Thạch,TP cẩm Phả Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0099567,
    "Latitude": 107.2570463
  },
  {
    "STT": 356,
    "Name": "\tNhà Thuốc Vân Hương\t",
    "address": "\tSN 48, ĐườngTrần Nhân Tông, Khu1,phường Thanh Sơn, Uông Bí, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0934902,
    "Latitude": 106.7935031
  },
  {
    "STT": 357,
    "Name": "\tQuầy Thuốc Doanh Nghiệp Số 37 - Công Ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tThôn Tân Yên, xã Hồng Thái Đông, huyện Đông Triều, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0391836,
    "Latitude": 106.7020879
  },
  {
    "STT": 358,
    "Name": "\tQuầy Thuốc Bích Ngọc - Công Ty TNHH Dược Phẩm Hồng Dương\t",
    "address": "\tXóm Chợ, xã Tiền An, thị xã Quảng Yên, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9386407,
    "Latitude": 106.8473377
  },
  {
    "STT": 359,
    "Name": "\tQuầy Thuốc Doanh Nghiệp Công Ngọc - Công Ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tThôn Xuân Quang, xã Yên Thọ, huyện Đông Triều, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0567778,
    "Latitude": 106.6202887
  },
  {
    "STT": 360,
    "Name": "\tQuầy Thuốc Chương Xuyến Số 5 - Công Ty TNHH Dược Phẩm Thương Mại Thái Ngọc\t",
    "address": "\tThôn Đồng Cao, xã Thống Nhất, huyện Hoành Bồ, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0351254,
    "Latitude": 107.0893798
  },
  {
    "STT": 361,
    "Name": "\tNhà thuốc Hương Giang\t",
    "address": "\tKi ốt 13 chợ Hạ Long 1, phường Bạch Đằng, Tphường Hạ Long, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9506862,
    "Latitude": 107.082389
  },
  {
    "STT": 362,
    "Name": "\tNhà thuốc Ngân Sơn 2\t",
    "address": "\tSN 46 tổ 77 khu 7, phường  Hà Khẩu, thành phố Hạ Long\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9736092,
    "Latitude": 106.9860384
  },
  {
    "STT": 363,
    "Name": "\tCông ty TNHHDP Hạ Long\t",
    "address": "\tSố 08 Phố Hoàng Long, phường Bạch Đằng, thành phố Hạ Long\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9508443,
    "Latitude": 107.0834804
  },
  {
    "STT": 364,
    "Name": "\tNhà thuốc Trung Tâm- Công ty CP Dược VTYT Q.Ninh\t",
    "address": "\tSố 703,  Lê Thánh Tông ,  TP Hạ Long\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9536147,
    "Latitude": 107.090891
  },
  {
    "STT": 365,
    "Name": "\tNHÀ THUỐC LAN ANH\t",
    "address": "\tSố nhà 82 Đường Nguyễn Văn Cừ, phường Hồng Hà, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9557688,
    "Latitude": 107.0980769
  },
  {
    "STT": 366,
    "Name": "\tQuầy thuốc doanh nghiệp\t",
    "address": "\tThôn Xuân Bình, xã Bình Khê ,  huyện Đông Triều ,  Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.1054044,
    "Latitude": 106.5854143
  },
  {
    "STT": 367,
    "Name": "\tQuầy thuốc doanh nghiệp\t",
    "address": "\tThôn 8, xã Hải Đông,  TP Móng Cái ,  Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.5394395,
    "Latitude": 107.8835642
  },
  {
    "STT": 368,
    "Name": "\tQuầy thuốc doanh nghiệp\t",
    "address": "\tThôn Thái Lập, xã Tân Lập,  Huyện Đầm Hà ,  Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.2762139,
    "Latitude": 107.5919395
  },
  {
    "STT": 369,
    "Name": "\tNhà thuốc doanh nghiệp số 01- C.ty TNHHDP Hạ Long\t",
    "address": "\tSố 8 phố Hoàng Long phường  Bạch Đằng TP Hạ Long\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9508443,
    "Latitude": 107.0834804
  },
  {
    "STT": 370,
    "Name": "\tQuầy thuốc số 28- C.ty TNHHDP Hải Bình\t",
    "address": "\tSN 260 tổ 4 khu Bắc Sơn, phường  Cẩm Sơn, TP Cẩm Phả\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.8437729,
    "Latitude": 106.2992912
  },
  {
    "STT": 371,
    "Name": "\tHộ kinh doanh thuốc đông y Đàm Oanh\t",
    "address": "\tSố nhà 24,  Phố Lê Hoàn, phường  Bạch Đằng, TP Hạ Long\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9509759,
    "Latitude": 107.0819854
  },
  {
    "STT": 372,
    "Name": "\tNhà thuốc Minh Hương- C.ty TNHHDP huyện Long\t",
    "address": "\tSN 400 tổ 64 khu 6 phường Cửa Ông, TP Cẩm Phả\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.032741,
    "Latitude": 107.3598781
  },
  {
    "STT": 373,
    "Name": "\tCông ty TNHHDP Bạch Đằng\t",
    "address": "\tTổ 36 khu 2 P Bạch Đằng, TP Hạ Long\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9513835,
    "Latitude": 107.0871951
  },
  {
    "STT": 374,
    "Name": "\tQuầy thuốc số 56- C.ty CP Dược VTYTQN\t",
    "address": "\tÔ B21 Chợ Trung Tâm Uông Bí,  TP Uông Bí\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0347844,
    "Latitude": 106.769557
  },
  {
    "STT": 375,
    "Name": "\tQuầy thuốc số 130- C.ty CP Dược VTYTQN\t",
    "address": "\tKhu Bình An, TT Bình Liêu, H Bình Liêu\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.5258306,
    "Latitude": 107.3957643
  },
  {
    "STT": 376,
    "Name": "\tQuầy thuốc số 123- C.ty CP Dược VTYTQN\t",
    "address": "\tTổ 6 khu 2, phường  Hồng Hà, Tp Hạ Long\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9443113,
    "Latitude": 107.1225746
  },
  {
    "STT": 377,
    "Name": "\tQuầy thuốcDN Hương Giang - C. ty TNHHDP huyện Long\t",
    "address": "\tTổ 1 khu 4 Phường Đại Yên thành phố Hạ Long\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9731011,
    "Latitude": 106.9322437
  },
  {
    "STT": 378,
    "Name": "\tNhà thuốc Minh Huyền\t",
    "address": "\tSN 68B Tổ 6 khu 4, Đường Bái Tử Long, phường Cẩm Trung, thành phố Cẩm Phả.\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0059966,
    "Latitude": 107.2724323
  },
  {
    "STT": 379,
    "Name": "\tQuầy thuốc số 123- C.ty CP Dược VTYTQN\t",
    "address": "\tTổ 6 khu 2, phường  Hồng Hà, Tp Hạ Long\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9443113,
    "Latitude": 107.1225746
  },
  {
    "STT": 380,
    "Name": "\tCông Ty Cổ Phần Công Nghệ Xanh Đông Sơn\t",
    "address": "\tSố 46 phố Anh Đào, Tổ 8 khu 2, phường Bãi Cháy, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9579074,
    "Latitude": 107.0226806
  },
  {
    "STT": 381,
    "Name": "\tNhà Thuốc Bình Huê\t",
    "address": "\tSố nhà 579, tổ 1 khu 4B, phường Quang Hanh, thành phố Cẩm Phả, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0082107,
    "Latitude": 107.2271147
  },
  {
    "STT": 382,
    "Name": "\tNhà Thuốc Tuấn Hoài\t",
    "address": "\tKi ốt B27, chợ Trung tâm Hải Hà, huyện Hải Hà, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.4498682,
    "Latitude": 107.7493787
  },
  {
    "STT": 383,
    "Name": "\tNhà Thuốc Vân Trang\t",
    "address": "\tSố nhà 09 tổ 2 khu 3, phường Hồng Hà, Tphường Hạ Long, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9417785,
    "Latitude": 107.1277751
  },
  {
    "STT": 384,
    "Name": "\tQuầy Thuốc Số 135- Công Ty Cổ Phần Dược Vtyt Quảng Ninh\t",
    "address": "\tXóm Bấc, xã Liên Vị, thị xã Quảng Yên, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9334638,
    "Latitude": 106.8414374
  },
  {
    "STT": 385,
    "Name": "\tCông Ty TNHH Một Thành Viên Dược Phẩm Trung Ương I- Chi Nhánh Quảng Ninh\t",
    "address": "\tSố 146, ngõ 3,  Đường Cao Thắng, phường Cao Thắng Tphường Hạ Long, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9628713,
    "Latitude": 107.0962345
  },
  {
    "STT": 386,
    "Name": "\tQuầy Thuốc Phương Thúy- Công Ty TNHH Dược Phẩm Hồng Dương\t",
    "address": "\tThôn Bình Lục Thượng, xã Hồng Phong, huyện Đông Triều,Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0743131,
    "Latitude": 106.5024344
  },
  {
    "STT": 387,
    "Name": "\tNhà Thuốc Thảo Hiền\t",
    "address": "\tTổ 69B khu 6, phường Cao Xanh, thành phố Hạ Long, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9793434,
    "Latitude": 107.0849499
  },
  {
    "STT": 388,
    "Name": "\tNhà Thuốc Hồng Hà - Công Ty Cổ Phần Dược Vtyt Quảng Ninh\t",
    "address": "\tSố 415A Đường Nguyễn văn Cừ, phường Hồng Hà, thành phố Hạ Long, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9443086,
    "Latitude": 107.1225384
  },
  {
    "STT": 389,
    "Name": "\tNhà Thuốc Số 1 - Công Ty TNHH Dược Phẩm Bạch Đằng\t",
    "address": "\tSố 71 đường 25/4, tổ 36 khu 2, phường Bạch Đằng, thành phố Hạ Long, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9502012,
    "Latitude": 107.0879032
  },
  {
    "STT": 390,
    "Name": "\tChi nhánh công ty cổ phần Traphaco\t",
    "address": "\tÔ số 1 Lô A6 Khu Đô thị mới Cao Xanh, P Hà Khánh, thành phố Hạ Long\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9643795,
    "Latitude": 107.0846676
  },
  {
    "STT": 391,
    "Name": "\tNhà thuốc Quý Kiên\t",
    "address": "\tSố 330 Đường Nguyễn Văn Cừ, P Hồng Hải,thành phố Hạ Long\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9495017,
    "Latitude": 107.1068609
  },
  {
    "STT": 392,
    "Name": "\tQuầy thuốc doanh nghiêp Hồng Phong\t",
    "address": "\tTổ 49 C khu 6, phường Cẩm Thành, TP Cẩm Phả\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0113744,
    "Latitude": 107.2769837
  },
  {
    "STT": 393,
    "Name": "\tĐại lý thuốc số 23\t",
    "address": "\tThôn An Biên 2, xã Lê Lợi, huyện Hoành Bồ\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0207142,
    "Latitude": 107.014153
  },
  {
    "STT": 394,
    "Name": "\tQuầy thuốc DN số 66- C.Ty TNHHDPHL\t",
    "address": "\tKhu Đường Ngang, phường  Minh Thành, Tx Quảng Yên\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.00493,
    "Latitude": 106.8552697
  },
  {
    "STT": 395,
    "Name": "\tQuầy Thuốc Số 43 - Công Ty Cổ phần Dược Vật Tư Y Tế Quảng Ninh\t",
    "address": "\tSố 144B ,  Hùng Vương, phường Ka Long, thành phố Móng Cái, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.531239,
    "Latitude": 107.9596089
  },
  {
    "STT": 396,
    "Name": "\tNhà Thuốc Minh Tâm 1\t",
    "address": "\tSố nhà 1139 Đường Trần Phú, Phường Cẩm Thạch, thành phố Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0101542,
    "Latitude": 107.2539561
  },
  {
    "STT": 397,
    "Name": "\tQuầy Thuốc Số 95 - Công Ty Cổ phần Dược Vtyt Quảng Ninh\t",
    "address": "\tSố 44, đường Quang Trung, thị trấn Tiên Yên, huyện Tiên Yên, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.3347973,
    "Latitude": 107.3937536
  },
  {
    "STT": 398,
    "Name": "\tNhà Thuốc Hoàng Sinh\t",
    "address": "\tSố 275 Đường Trần Quốc Tảng,phường cẩm Thịnh, TP Cẩm Phả\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0080058,
    "Latitude": 107.3445192
  },
  {
    "STT": 399,
    "Name": "\tNhà Thuốc Thắng Hạnh\t",
    "address": "\tSố 104 Nguyễn Bình, phường Quảng Yên, thị xã Quảng Yên, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9392509,
    "Latitude": 106.8137728
  },
  {
    "STT": 400,
    "Name": "\tQuầy Thuốc Số 68 - Công Ty Cp Dược Vtyt Quảng Ninh\t",
    "address": "\tSố 494 Đường Nguyễn Văn Cừ, phường Hồng Hà, Tphường  Hạ Long, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9482282,
    "Latitude": 107.1293532
  },
  {
    "STT": 401,
    "Name": "\tCông ty TNHHMTV Thảo Châm\t",
    "address": "\tSN 789 Đường Nguyễn Văn Cừ,  phường Hồng Hải Tphường  HL\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9481232,
    "Latitude": 107.1074036
  },
  {
    "STT": 402,
    "Name": "\tQuầy thuốc Thảo Châm\t",
    "address": "\tSN 789 Đường Nguyễn Văn Cừ,  phường  Hồng Hải thành phố Hạ Long\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9481232,
    "Latitude": 107.1074036
  },
  {
    "STT": 403,
    "Name": "\tNhà thuốc Hương Xuyên\t",
    "address": "\tTổ 108 K6,  phường  B Đằng , thành phố Hạ Long\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9711977,
    "Latitude": 107.0448069
  },
  {
    "STT": 404,
    "Name": "\tNhà thuốc Hồng Dương - C.ty TNHHDP Hồng Dương\t",
    "address": "\tSố 1, Tổ 1 khu 1,phường Trần Hưng Đạo,  thành phố Hạ Long,Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9533239,
    "Latitude": 107.0871119
  },
  {
    "STT": 405,
    "Name": "\tNhà thuốc Đông Đô - Công ty Cổ phần Y Tế Đông Đô\t",
    "address": "\tSố 315 Cầu sến, phường Yên Thanh, Tphường  Uông Bí, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0352525,
    "Latitude": 106.7409083
  },
  {
    "STT": 406,
    "Name": "\tQuầy thuốc Doanh Nghiệp Quang Minh- Công ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tKhu Yên Hợp, xã Yên Thọ, huyện Đông Triều, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0585004,
    "Latitude": 106.6122025
  },
  {
    "STT": 407,
    "Name": "\tQuầy thuốc Doanh Nghiệp Số 18 - Công ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tThôn Đồng Tâm, xã Lê Lợi, huyện Hoành Bồ, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0195827,
    "Latitude": 107.0358077
  },
  {
    "STT": 408,
    "Name": "\tQuầy thuốc Doanh Nghiệp Số 4 Linh Chi - Công ty TNHH Dược Phẩm Thương Mại Thái Ngọc\t",
    "address": "\tThôn 4, xã Sông Khoai, thị xã Quảng Yên, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9768581,
    "Latitude": 106.8119382
  },
  {
    "STT": 409,
    "Name": "\tQuầy thuốc Doanh Nghiệp Thảo Hiền - Công ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tThôn 3, xã Đức Chính, huyện Đông Triều, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.08051,
    "Latitude": 106.5256786
  },
  {
    "STT": 410,
    "Name": "\tQuầy thuốc Doanh Nghiệp Số 112 - Công ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tThôn Đông Hải, xã Đông Xá, huyện Vân Đồn, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0600411,
    "Latitude": 107.4170053
  },
  {
    "STT": 411,
    "Name": "\tNhà thuốc Duyên Thùy\t",
    "address": "\tSố nhà 384, Đường Trần Phú, phường Cẩm Trung, thành phố Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0097891,
    "Latitude": 107.270649
  },
  {
    "STT": 412,
    "Name": "\tNhà thuốc Số 1A - Công ty TNHH Dược Phẩm Bạch Đằng\t",
    "address": "\tSố 12 Lê Quý Đôn, phường Bạch Đằng, thành phố Hạ Long, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9505075,
    "Latitude": 107.0808706
  },
  {
    "STT": 413,
    "Name": "\tNhà thuốc Trung Thành -\t",
    "address": "\tKi ốt số 6 vành đai nhà tôn chợ Giếng Đáy, phường Giếng Đáy, thành phố Hạ Long, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 24.2225023,
    "Latitude": 120.6545022
  },
  {
    "STT": 414,
    "Name": "\tQuầy thuốc Doanh Nghiệp Thanh Bình - Công ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tSố nhà 265, khu 3, thị trấn Cái Rồng, huyện Vân Đồn, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.053875,
    "Latitude": 107.4351532
  },
  {
    "STT": 415,
    "Name": "\tQuầy thuốc Doanh Nghiệp Thanh Huyên 2 - Công ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tThôn Cửa Tràng, xã Tiền An, thị xã Quảng Yên, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9385813,
    "Latitude": 106.8316791
  },
  {
    "STT": 416,
    "Name": "\tQuầy thuốc Doanh Nghiệp Số 75 - Công ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tThôn 8, xã Hải Đông, Tphường Móng Cái, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.5394395,
    "Latitude": 107.8835642
  },
  {
    "STT": 417,
    "Name": "\tQuầy thuốc Doanh Nghiệp Ngọc Thủy - Công ty TNHH Dược Phẩm Hồng Dương\t",
    "address": "\tĐoàn xã 1, xã Hồng Phong, huyện Đông Triều, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0743131,
    "Latitude": 106.5024344
  },
  {
    "STT": 418,
    "Name": "\tNhà thuốc Thanh Nhàn 2\t",
    "address": "\tKi ốt 188A Chợ Cẩm Đông, phường Cẩm Đông, thành phố Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0081551,
    "Latitude": 107.2919065
  },
  {
    "STT": 419,
    "Name": "\tNhà thuốc Bệnh Viện Lao Và Phổi\t",
    "address": "\tBệnh viện Lao và Phổi, phường Cao Xanh, thành phố Hạ Long, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.979084,
    "Latitude": 107.0900325
  },
  {
    "STT": 420,
    "Name": "\tNhà thuốc Hương Trang\t",
    "address": "\tTổ 11 khu 4, phường Hà Lầm, thành phố Hạ Long, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.970539,
    "Latitude": 107.1144837
  },
  {
    "STT": 421,
    "Name": "\tQuầy thuốc Doanh Nghiệp Số 12 - Công ty TNHH Dược Phẩm Bạch Đằng\t",
    "address": "\tĐịa chỉ Tổ 4 khu 7A, Phường Quang Hanh, thành phố Cẩm Phả, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9955298,
    "Latitude": 107.2084093
  },
  {
    "STT": 422,
    "Name": "\tNhà thuốc Minh Tuyến\t",
    "address": "\tSố nhà 526 ,  Đường Nguyễn Văn Cừ, phường Hồng Hải, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9557688,
    "Latitude": 107.0980769
  },
  {
    "STT": 423,
    "Name": "\tNhà thuốc Thanh Thúy\t",
    "address": "\tKhu 7, phường Hải Yên, thành phố Móng Cái, quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.5259366,
    "Latitude": 107.9279309
  },
  {
    "STT": 424,
    "Name": "\tQuầy thuốc Doanh Nghiệp Số 44 - Công ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tSố 25,  Lý Thường Kiệt, thị trấn Tiên Yên, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.3315865,
    "Latitude": 107.4025342
  },
  {
    "STT": 425,
    "Name": "\tNhà thuốc Long Hiền\t",
    "address": "\tSố 270 Đường Vũ Văn Hiếu, phường Hà Tu, thành phố Hạ Long, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9585581,
    "Latitude": 107.1526733
  },
  {
    "STT": 426,
    "Name": "\tNhàthuốc Doanh Nghiệp Minh Phương - Công ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tSố 102, tổ 15 khu 3, phường Cửa Ông, Tphường Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.032741,
    "Latitude": 107.3598781
  },
  {
    "STT": 427,
    "Name": "\tCơ Sở thuốc Y Học Cổ Truyền\t",
    "address": "\tKi ốt số 1 chợ Ba Lan, phường Giếng Đáy, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9814933,
    "Latitude": 107.0232489
  },
  {
    "STT": 428,
    "Name": "\tNhà thuốc Số 1 Hà Nội\t",
    "address": "\tSố 140 phố 1, phường Mạo Khê, thị xã Đông Triều, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.058394,
    "Latitude": 106.5934706
  },
  {
    "STT": 429,
    "Name": "\tNhà thuốc Hùng Mạnh\t",
    "address": "\tSố 238, tổ 12A, khu 4, phường Vàng Danh, thành phố Uông Bí, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.097721,
    "Latitude": 106.8016144
  },
  {
    "STT": 430,
    "Name": "\tNhà thuốc Thanh Nhàn\t",
    "address": "\tSố 351, phố Mới, phường  Cửa Ông, Tphường  Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.032741,
    "Latitude": 107.3598781
  },
  {
    "STT": 431,
    "Name": "\tNhà thuốc Tâm Đức\t",
    "address": "\tSố nhà 63, tổ 1 khu Minh Khai, phường Cẩm Tây, thành phố Cẩm Phả, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0120178,
    "Latitude": 107.2872176
  },
  {
    "STT": 432,
    "Name": "\tQuầy thuốc Doanh Nghiệp Anh Đào - Công ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tKhu Dốc Thụ, xã Yên Thọ, huyện Đông Triều, Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0567778,
    "Latitude": 106.6202887
  },
  {
    "STT": 433,
    "Name": "\tQuầy thuốc Doanh Nghiệp Hồng Vân - Công ty TNHH Dược Phẩm Hồng Dương\t",
    "address": "\tThôn 13, xã Hạ Long, huyện Vân Đồn, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0623461,
    "Latitude": 107.4913489
  },
  {
    "STT": 434,
    "Name": "\tQuầy thuốc Doanh Nghiệp Số 135 - Công ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tSố 183, tổ 1 khu Quang Trung, thị trấn Mạo Khê, huyện Đông Triều, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0584203,
    "Latitude": 106.5985501
  },
  {
    "STT": 435,
    "Name": "\tNhà thuốc Bệnh Viện Sản Nhi\t",
    "address": "\tPhường Đại Yên, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 20.9828115,
    "Latitude": 106.9240554
  },
  {
    "STT": 436,
    "Name": "\tNhà thuốc Xuân Thu - Công ty TNHH Dược Phẩm Hạ Long\t",
    "address": "\tSố nhà 41 phố Nguyễn Du, phường Quang Trung, thành phố Uông Bí, tỉnh Quảng Ninh\t",
    "StartTime": "\t7:00:00 AM\t",
    "EndTime": "\t10:00:00 PM\t",
    "Longtitude": 21.0378074,
    "Latitude": 106.7701049
  }
];